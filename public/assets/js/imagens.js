function initLazyLoad() {
    $('img.lazy-load').each(handleLazyLoad)
}

function handleLazyLoad(index, value) {
    var src = $(value).data('real-src');
    $(value).attr('src', src);
    $(value).removeClass('lazy-load');
}

function handleImageError(event) {
    $(event.target).attr('src', defaultImage);
}

const defaultImage = '/assets/imagens/blur.jpg';

$(function() {
    initLazyLoad();

    $('img').on('error', handleImageError);
});