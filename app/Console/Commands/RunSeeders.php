<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use Modules\Admin\Providers\AdminServiceProvider;
use Modules\Support\src\Supports\Helper;

class RunSeeders extends Command
{
    protected $signature = 'run-seeders';

    protected $description = 'Comando para executar todos os seeders dos módulos';

    protected $quantidadeExecutado;

    protected $arquivoAtual;

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        try {
            $this->laravel->register(AdminServiceProvider::class);

            DB::beginTransaction();
            $arquivos = $this->inicializarArquivos();
            $this->percorrerArquivos($arquivos);
            DB::commit();

            if (!$this->quantidadeExecutado) {
                $this->info('Não tem nenhum arquivo novo para executar!');
            }
            
        } catch (\Exception $e) {
            DB::rollBack();
            $this->comment(sprintf('Não foi possível executar os seeders: %s --- em: %s', $e->getMessage(), $this->arquivoAtual));
            die;
        }
    }

    private function inicializarArquivos()
    {
        $caminho = base_path('Modules/Admin/database/seeds');
        $arquivos = scandir($caminho);
        Helper::autoload($caminho);

        return $arquivos;
    }

    private function percorrerArquivos($arquivos)
    {
        $this->quantidadeExecutado = 0;
        
        foreach ($arquivos as $arquivo) {
            if (strlen($arquivo) < 3) {
                continue;
            }
            
            $arquivoJaExecutado = $this->validarSeederExecutado($arquivo);
            if (!$arquivoJaExecutado) {
                $this->arquivoAtual = $arquivo;
                $this->executarSeeder($arquivo);
            }
        }
    }

    private function validarSeederExecutado($arquivo)
    {
        try {
            $seed = DB::table('seeds')->where('arquivo', $arquivo)->first();
            return $seed;
        } catch (\Exception $e) {
            $this->comment(sprintf('Não foi possível validar o arquivo: %s, as migrations foram executadas?', $arquivo));
            die;
        }
    }

    private function executarSeeder($arquivo)
    {
        $nomeDaClasse = $this->gerarNomeDaClasse($arquivo);
        $namespace = sprintf('Modules\\Admin\\Database\\Seeds\\%s', $nomeDaClasse);

        $classe = $this->laravel->make($namespace);
        $classe->setContainer($this->laravel)->setCommand($this)->__invoke();
        
        $this->registrarExecucaoDoSeeder($arquivo);
        $this->info(sprintf('Seeder executada: %s', $arquivo));
    }

    private function gerarNomeDaClasse($nome)
    {
        $nomeDaClasse = camel_case($nome);
        $nomeDaClasse = preg_replace('/[0-9]+/', '', $nomeDaClasse);
        
        $nomeDaClasse = str_replace('.php', '', $nomeDaClasse);
        $nomeDaClasse = ucfirst($nomeDaClasse);

        return $nomeDaClasse;
    }

    private function registrarExecucaoDoSeeder($arquivo)
    {
        DB::table('seeds')->insert([
            'arquivo' => $arquivo,
        ]);

        $this->quantidadeExecutado++;
    }
}
