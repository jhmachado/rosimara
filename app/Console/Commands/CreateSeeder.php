<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

/**
 * Class CreateSeeder
 *
 * @package App\Console\Commands
 */
class CreateSeeder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create-seeder {nome}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para gerar um novo seed no formato esperado';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $nome = $this->argument('nome');
            
            $nomeDaClasse = $this->gerarNomeDaClasse($nome);
            $destino = $this->gerarCaminhoDoArquivo($nome);
            
            $this->duplicarArquivo($nomeDaClasse, $destino);
            $this->info(sprintf('Classe %s criada em %s', $nomeDaClasse, $destino));

        } catch (Exception $e) {
            $this->info("Não foi possível criar o arquivo: " . $e->getMessage());
        }
    }

    /**
     * Método usado para gerar o nome de uma classe, no formato camelCase, a partir
     * do nome digitado pelo usuário, usando o helper camel_case do laravel.
     *
     * @param string $nome
     *
     * @return string
     */
    private function gerarNomeDaClasse($nome)
    {
        $nomeDaClasse = camel_case($nome);
        $nomeDaClasse = ucfirst($nomeDaClasse);
        return $nomeDaClasse;
    }

    /**
     * Método usado para gerar o novo seeder a partir de um arquivo padrão já existente,
     * usando o nome da classe já gerado e o destino desejado.
     *
     * @param string $nomeDaClasse
     * @param string $destino
     *
     * @return void
     */
    private function duplicarArquivo($nomeDaClasse, $destino)
    {
        $conteudoPadrao = file_get_contents(__DIR__.'/base_seeder.txt');
        $conteudoFinal = str_replace('nomeDoSeeder', $nomeDaClasse, $conteudoPadrao);

        file_put_contents($destino, $conteudoFinal);
    }

    /**
     * Método usado para gerar o caminho total do arquivo, baseado na base_path do projeto
     * junto do nome digitado.
     * 
     * @param string $nome
     * 
     * @return string
     */
    private function gerarCaminhoDoArquivo($nome)
    {
        $localBase = base_path('Modules/Admin/database/seeds/');
        $caminhoDoArquivo = sprintf('%s%s_%s_%s.php', $localBase, date('Y_m_d'), time(), $nome);
        return $caminhoDoArquivo;
    }
}
