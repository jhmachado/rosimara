<div class="card__imovel text-center">
	<a href="{{ route('site.slug', ['slug' => $imovel->slug]) }}">
		<div class="card__imovel-imagem">
			<span class="tag tag__opaco"> {{ $imovel->tipoImovel->titulo }} </span>
			<img
				class="lazy-load"
				alt="{{ $imovel->titulo }}"
				src="{{ url('/assets/imagens/blur.jpg') }}"
				data-real-src="{{ route('site.imagens', ['tipo' => 'imovel', 'imagem' => $imovel->fotoPrincipal]) }}"
			/>

			@if ($imovel->destaque)
				<span class="tag tag__escura"> Destaque </span>
			@endif

			<span class="tag tag__rosa"> {{ $imovel->tipoNegocio->titulo }} </span>
		</div>

		<div class="card__imovel-detalhes">
			<h3> {{ $imovel->titulo }} </h3>
			<p>
				{{ str_limit($imovel->descricao, 120) }}
			</p>

			<p class="m-0">
				<i class="fas fa-map-marker-alt"></i>
				{{ $imovel->endereco }}
			</p>
		</div>
	</a>

	<a href="{{ route('site.slug', ['slug' => $imovel->slug]) }}">
		<div class="card__imovel-link">
			Ver imóvel
		</div>
	</a>
</div>