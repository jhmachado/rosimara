<section class="banner">
    @foreach($banners as $banner)
        <div class="banner__item">
            <figure>
                <img src="{{ route('site.imagens', ['tipo' => 'banner', 'imagem' => $banner->imagem]) }}">
                <figcaption>
                    <a href="{{ $banner->url }}">
                        <div class="banner__detalhes">
                            {{ $banner->texto }}
                        </div>
                    </a>
                </figcaption>
            </figure>
        </div>
    @endforeach

    <div class="banner__scroll">
        <a href="#pesquisa"><span></span></a>
    </div>
</section>