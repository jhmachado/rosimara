<div class="col-sm-12 mt-10 text-center">
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            @if ($exibirLinkParaPrimeiraPagina)
                <li class="page-item">
                    <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
            @endif

            @for($primeiroLink; $primeiroLink < $ultimoLink; $primeiroLink++)
                <li class="page-item"><a class="page-link" href="#">1</a></li>
            @endfor

            @if ($exibirLinkParaUltimaPagina)
                <li class="page-item">
                    <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
            @endif
        </ul>
    </nav>
</div>