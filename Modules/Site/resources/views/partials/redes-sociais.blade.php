<ul class="menu__horizontal">
    @foreach ($redesSociais as $redeSocial)
        <li class="menu__horizontal-item">
            <a href="{{ $redeSocial->url }}">
                <i class="{{ $redeSocial->icone }}"></i>
                <span class="icone" data-width="32" data-jam="{{ $redeSocial->icone }}" data-fill="#222"></span>
            </a>
        </li>
    @endforeach
</ul>