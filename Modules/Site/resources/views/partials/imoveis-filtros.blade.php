<div class="imoveis__filtros my-10 p-10">
    <form action="{{ route('site.imoveis') }}" method="post">

        <div class="imoveis__filtros-sessao">
            <h3> Filtros </h3>
        </div>

        <div class="imoveis__filtros-sessao">
            <div class="form-group">
                <label for="negocio">Tipo de negócio</label>
                <select class="form-control" name="negocio" id="negocio">
                    <option value="0"> Todos </option>
                    @foreach($negocios as $negocio)
                        <option value="{{ $negocio->id }}" {{ ($filtros['negocio'] == $negocio->id) ? 'selected="selected"' : '' }}>
                            {{ $negocio->titulo }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="imoveis__filtros-sessao">
            <div class="form-group">
                <label for="tipo">Tipo de imóvel</label>
                <select class="form-control" name="tipo" id="tipo">
                    <option value="0"> Todos </option>
                    @foreach($tiposDeImoveis as $tipoDeImovel)
                        <option value="{{ $tipoDeImovel->id }}" {{ ($filtros['tipo'] == $tipoDeImovel->id) ? 'selected="selected"' : '' }}>
                            {{ $tipoDeImovel->titulo }}
                        </option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="imoveis__filtros-sessao">
            <h3> Localização </h3>
            <div class="form-group">
                <label for="estado"> Estado </label>
                <select class="form-control" name="estado" id="estado">
                    <option value="0"> Todos </option>
                    @foreach($estados as $uf => $estado)
                        <option value="{{ $uf}}" {{ ($filtros['estado'] == $uf) ? 'selected="selected"' : '' }}>
                            {{ $estado }}
                        </option>
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="endereco"> Endereço </label>
                <input type="text" class="form-control" id="endereco" name="endereco" placeholder="Endereço" value="{{ old('endereco', $filtros['endereco'])  }}">
            </div>
        </div>

        <div class="imoveis__filtros-sessao">
            <button type="submit" class="btn btn-primary">
                Pesquisar
            </button>
        </div>
    </form>
</div>