@if ($imoveis->count() > 0)

    @if (!empty($titulo) && !empty($linkParaListagem))
        <div class="col-sm-12 text-center titulo-secundario mt-10 mb-10">
            <a href="{{ $linkParaListagem }}">
                <h2> {{ $titulo }} </h2>
            </a>
        </div>
    @endif

    @foreach($imoveis as $imovel)
        <div class="{{ $tamanhoDaColuna }}">
            @include('site::components.imovel', $imovel)
        </div>
    @endforeach

    @if (!empty($linkParaListagem))
        <div class="col-sm-12 mt-10 text-center">
            <a href="{{ $linkParaListagem }}" class="btn btn-primary mt-5 mw-2">
                Ver todos
            </a>
        </div>
    @endif

    @if ($paginacao ?? false)
        {!!
            PartialViews::carregarViewParcial([
                'path' => 'site::partials.paginacao',
                'data' => $paginacao ?? null
            ])
        !!}
    @endif
@endif
