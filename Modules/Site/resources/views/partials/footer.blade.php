<footer class="py-10">
	<div class="container">
		<div class="row">
			
			<div class="col-md-3">
				<a href="#home">
					<h1> LOGO </h1>
					<p> Número do telefone </p>
					<p> E-mail de contato </p>
				</a>

				{!!
                    PartialViews::carregarViewParcial([
                        'path' => 'site::partials.redes-sociais'
                    ])
                !!}
			</div>
			
			<div class="col"></div>

			<div class="col-md-5">
				<h3> SOBRE MIM </h3>
				<p>
					Lorem Ipsum é simplesmente uma simulação de texto da indústria tipográfica e de impressos, e vem sendo utilizado desde o século XVI, quando um impressor desconhecido pegou uma bandeja de tipos e os embaralhou para fazer um livro de modelos de tipos. Lorem Ipsum sobreviveu não só a cinco séculos, como também ao salto para a editoração eletrônica, permanecendo essencialmente inalterado. Se popularizou na década de 60, quando a Letraset lançou decalques contendo passagens de Lorem Ipsum, e mais recentemente quando passou a ser integrado a softwares de editoração eletrônica como Aldus PageMaker.
				</p>
			</div>
			
			<div class="col"></div>

			<div class="col-md-auto">
				<h3> LINKS RÁPIDOS </h3>
				<ul class="lista">
					<li class="lista__item">
						<a href="#seja-um-cliente">
							<p class="m-0">
								Seja um cliente
							</p>
						</a>
					</li>

					<li class="lista__item">
						<a href="#entre-em-contato">
							<p class="m-0">
								Entre em contato
							</p>
						</a>
					</li>

					<li class="lista__item">
						<a href="#duvidas-frequentes">
							<p class="m-0">
								Dúvidas frequentes
							</p>
						</a>
					</li>

				</ul>
			</div>

		</div>
	</div>
</footer>