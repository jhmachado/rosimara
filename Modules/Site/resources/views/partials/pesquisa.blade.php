<section id="pesquisa">
    <div class="container">
        <div class="pesquisa__box">
            <form action="#pesquisa" method="post">

                <div class="row">
                    <div class="col-sm-12 text-center titulo-secundario mb-10">
                        <a href="#servicos">
                            <h2> Pesquisar rápida </h2>
                        </a>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="form-group">
                            <label for="tipo-de-negocio">Tipo de negócio</label>
                            <select class="form-control" id="tipo-de-negocio" name="tipo_negocio">
                                <option> Todos </option>
                                @foreach($negocios as $negocio)
                                    <option value="{{ $negocio->id }}"> {{ $negocio->titulo }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
i
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="form-group">
                            <label for="tipo-de-imovel">Tipo de imóvel</label>
                            <select class="form-control" id="tipo-de-imovel" name="tipo_imovel">
                                <option> Todos </option>
                                @foreach($tiposDeImoveis as $tipoImovel)
                                    <option value="{{ $tipoImovel->id }}"> {{ $tipoImovel->titulo }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="form-group">
                            <label for="estado"> Estado </label>
                            <select class="form-control" id="estado" name="estado">
                                <option> Todos </option>
                                @foreach($estados as $uf => $estado)
                                    <option value="{{ $uf }}"> {{ $estado }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="form-group">
                            <label for="cidade"> Cidade </label>
                            <select class="form-control" id="cidade" name="cidade" disabled>
                                <option> Selecione um estado </option>
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="form-group">
                            <label for="bairro"> Bairro </label>
                            <select class="form-control" id="bairro" name="bairro" disabled>
                                <option> Selecione uma cidade </option>
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="form-group">
                            <label for="valor-minimo"> Valor mínimo </label>
                            <select class="form-control" id="valor-minimo" name="valor_minimo">
                                <option> Valor 1 </option>
                                <option> Valor 2 </option>
                                <option> Valor 3 </option>
                                <option> Valor 4 </option>
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="form-group">
                            <label for="valor-maximo"> Valor máximo </label>
                            <select class="form-control" id="valor-maximo" name="valor-maximo">
                                <option> Valor 1 </option>
                                <option> Valor 2 </option>
                                <option> Valor 3 </option>
                                <option> Valor 4 </option>
                            </select>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                        <label><p></p></label>
                        <button type="submit" class="btn btn-primary mwp-10">
                            Pesquisar
                        </button>
                    </div>
                </div>

            </form>
        </div>
    </div>
</section>