<header>
	<nav class="navbar navbar-expand-lg navbar-light header__navbar">
		<div class="container">
		  	<a class="navbar-brand" href="{{ route('site.home') }}"> LOGO </a>
		  	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu" aria-controls="menu" aria-expanded="false" aria-label="Exibir menu">
		    	<span class="navbar-toggler-icon"></span>
		  	</button>

			<div class="collapse navbar-collapse justify-end" id="menu">
				<ul class="navbar-nav">
					<li class="header__nav-item active">
						<a class="header__nav-link" href="#">
							HOME
						</a>
					</li>
					
					<li class="header__nav-item">
						<a class="header__nav-link" href="{{ route('site.slug', ['slug' => 'venda']) }}">VENDA</a>
					</li>
					
					<li class="header__nav-item">
						<a class="header__nav-link" href="{{ route('site.slug', ['slug' => 'locacao']) }}">LOCAÇÃO</a>
					</li>
					
					<li class="header__nav-item">
						<a class="header__nav-link" href="{{ route('site.slug', ['slug' => 'lancamentos']) }}">LANÇAMENTOS</a>
					</li>
					
					<li class="header__nav-item">
						<a class="header__nav-link" href="{{ route('site.contato') }}">CONTATO</a>
					</li>

				</ul>
			</div>
		</div>
	</nav>
</header>