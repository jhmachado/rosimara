@extends('site::layouts.master')

@section('content')
    <section id="imovel">
        <div class="container">
            <div class="row">

                <div class="col-sm-10">
                    <div class="imovel__titulo py-5">
                        <h1> {{ $imovel->titulo }} </h1>
                        <p> {{ $imovel->endereco }} </p>
                    </div>
                </div>

                <div class="col-sm-2 py-5 text-right">
                    <div class="imovel__interacao">
                        <button class="btn btn-default">
                            <i></i>
                        </button>

                        <button class="btn btn-default">
                            <i></i>
                        </button>

                        <button class="btn btn-default">
                            <i></i>
                        </button>
                    </div>

                    <div class="imovel__preco">
                        {{ $imovel->preco }}
                    </div>
                </div>
            </div>

            <div class="imovel__banner">
                <div class="imovel__banner-imagem-ativa">
                    <figure>
                        <img
                                class="lazy-load object-fit"
                                alt="{{ $imovel->titulo }}"
                                src="{{ url('/assets/imagens/blur.jpg') }}"
                                data-real-src="{{ route('site.imagens', ['tipo' => 'imovel', 'imagem' => $imovel->fotoPrincipal]) }}"
                        />
                    </figure>
                </div>
                <div class="imovel__banner-miniaturas">
                    <div class="flex">
                        @foreach($imovel->fotos as $fotoImovel)
                            <div class="imovel__banner-miniatura-imagem mt-1 p-0 mr-1 col-sm-1">
                                <figure>
                                    <img
                                            class="lazy-load object-fit"
                                            alt="{{ $imovel->titulo }}"
                                            src="{{ url('/assets/imagens/blur.jpg') }}"
                                            data-real-src="{{ route('site.imagens', ['tipo' => 'imovel', 'imagem' => $fotoImovel->foto->url]) }}"
                                    />
                                </figure>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="flex space-between">
                <div class="imovel__descricao my-5 p-8">
                    <h2 class="subtitulo"> Descrição </h2>
                    {{ $imovel->descricao }}
                </div>

                <div class="imovel__contato my-5 p-8">
                    <h3 class="subtitulo"> Entre em contato</h3>

                    <form action="">
                        <div class="form-group">
                            <label for="nome"> Seu nome </label>
                            <input type="text" class="form-control" name="nome" id="nome" value="{{ old('nome', $nome ?? '') }}">
                        </div>

                        <div class="form-group">
                            <label for="telefone"> Telefone </label>
                            <input type="text" class="form-control" name="telefone" id="telefone" value="{{ old('telefone', $telefone ?? '') }}">
                        </div>

                        <div class="form-group">
                            <label for="email"> E-mail </label>
                            <input type="email" class="form-control" name="email" id="email" value="{{ old('email', $email ?? '') }}">
                        </div>

                        <div class="form-group">
                            <label for="mensagem"> Mensagem </label>
                            <textarea class="form-control" name="mensagem" id="mensagem">{{ old('mensagem', $mensagem ?? '')  }}</textarea>
                        </div>

                        <button type="submit" class="btn btn-primary miwp-10">
                            ENVIAR
                        </button>
                    </form>
                </div>

                <div class="imovel__endereco my-5 p-8">
                    <h3 class="subtitulo"> Localização </h3>

                    <div class="flex">
                        <div class="col-sm-5 p-0 mr-1">
                            <p> <b> Endereço: </b> {{ $imovel->endereco }} </p>
                        </div>

                        <div class="col-sm-5 p-0 mr-1">
                            <p> <b> Cidade: </b> {{ $imovel->cidade->nome }} / {{ $imovel->cidade->uf }}</p>
                        </div>

                        @if ($imovel->bairro)
                            <div class="col-sm-5 p-0 mr-1">
                                <p> <b> Bairro: </b> {{ $imovel->bairro->nome }}</p>
                            </div>
                        @endif

                        @if (!empty($imovel->cep))
                            <div class="col-sm-5 p-0 mr-1">
                                <p> <b> CEP: </b> {{ $imovel->cep }}</p>
                            </div>
                        @endif
                    </div>
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3660.3276026962635!2d-51.96320758455715!3d-23.44864468473897!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ecd7697bd0959f%3A0xab452fd9292e47bb!2sR.+Pioneira+Regina+Marson+Badan%2C+707+-+Jardim+Igua%C3%A7%C3%BA%2C+Maring%C3%A1+-+PR%2C+87060-160!5e0!3m2!1spt-BR!2sbr!4v1543711171319" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>

                <div class="imovel__detalhes my-5 p-8">
                    @if ($imovel->detalhes->count() > 0)
                        <h3 class="subtitulo"> Detalhes do imóvel </h3>
                        <div class="alert alert-primary py-5 flex">
                            @foreach($imovel->detalhes as $detalhe)
                                <div class="col-sm-4 p-0">
                                    <p> <b> {{ $detalhe->titulo }} </b> - {{$detalhe->valor }}</p>
                                </div>
                            @endforeach
                        </div>
                        <br><br>
                    @endif
                    <h4 class="subtitulo"> Detalhes de negócio </h4>
                    <p> <b> Tipo de negócio: </b> {{ $imovel->negocio->titulo }}</p>
                    <p> <b> Tipo de imóvel: </b> {{ $imovel->tipoImovel->titulo }}</p>
                </div>
            </div>

        </div>
    </section>
@endsection
