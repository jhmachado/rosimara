@extends('site::layouts.master')

@section('content')
	{!! PartialViews::carregarViewParcial([ 'path' => 'site::partials.banner' ]) !!}

	<div class="container">
		<div class="row">
			{!!
				PartialViews::carregarViewParcial([
					'path' => 'site::partials.section-imoveis',
					'data' => [
						'titulo' => 'Imóveis adicionados recentemente',
						'linkParaListagem' => route('site.slug', ['slug' => '']),
						'imoveis' => $adicionadosRecentemente,
						'tamanhoDaColuna' => 'col-xs-12 col-sm-6 col-md-6 col-lg-3',
					]
				])
			!!}
		</div>
	</div>

	{{--<div class="container">--}}
		{{--<div class="row">--}}
			{{--{!!--}}
				{{--PartialViews::carregarViewParcial([--}}
					{{--'path' => 'site::partials.section-imoveis',--}}
					{{--'data' => [--}}
						{{--'titulo' => 'Destaques de locação',--}}
						{{--'linkParaListagem' => route('site.slug', ['slug' => 'locacao']),--}}
						{{--'imoveis' => $destaquesDeLocacao,--}}
						{{--'tamanhoDaColuna' => 'col-xs-12 col-sm-6 col-md-6 col-lg-3',--}}
					{{--]--}}
				{{--])--}}
			{{--!!}--}}
		{{--</div>--}}
	{{--</div>--}}

	{{--<div class="container">--}}
		{{--<div class="row">--}}
			{{--{!!--}}
				{{--PartialViews::carregarViewParcial([--}}
					{{--'path' => 'site::partials.section-imoveis',--}}
					{{--'data' => [--}}
						{{--'titulo' => 'Destaques de venda',--}}
						{{--'linkParaListagem' => route('site.slug', ['slug' => 'venda']),--}}
						{{--'imoveis' => $destaquesDeVenda,--}}
						{{--'tamanhoDaColuna' => 'col-xs-12 col-sm-6 col-md-6 col-lg-3',--}}
					{{--]--}}
				{{--])--}}
			{{--!!}--}}
		{{--</div>--}}
	{{--</div>--}}

	{{--<section id="servicos" class="pt-1 pb-10 mt-10 servicos">--}}
		{{--<div class="container">--}}
			{{--<div class="row">--}}
				{{--<div class="col-sm-12 text-center titulo-secundario mt-10 mb-10">--}}
					{{--<a href="#servicos">--}}
						{{--<h2 class="text-white"> Serviços </h2>--}}
						{{--<p class="text-white m-0">--}}
							{{--Lorem Ipsum is simply dummy text of the printing and typesetting industry--}}
						{{--</p>--}}
					{{--</a>--}}
				{{--</div>--}}
				{{----}}
				{{--<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">--}}
					{{--<div class="card card__blank text-center">--}}
						{{--<a href="#servico-detalhes">--}}
							{{--<div class="card__icone">--}}
								{{--<i class="fas fa-home"></i>--}}
							{{--</div>--}}

							{{--<h3> Serviço 1 </h3>--}}
							{{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>--}}
						{{--</a>--}}
					{{--</div>--}}
				{{--</div>--}}
				{{----}}
				{{--<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">--}}
					{{--<div class="card card__blank text-center">--}}
						{{--<a href="#servico-detalhes">--}}
							{{--<div class="card__icone">--}}
								{{--<i class="fas fa-comments"></i>--}}
							{{--</div>--}}

							{{--<h3> Serviço 2 </h3>--}}
							{{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>--}}
						{{--</a>--}}
					{{--</div>--}}
				{{--</div>--}}

				{{--<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">--}}
					{{--<div class="card card__blank text-center">--}}
						{{--<a href="#servico-detalhes">--}}
							{{--<div class="card__icone">--}}
								{{--<i class="fas fa-lock"></i>--}}
							{{--</div>--}}

							{{--<h3> Serviço 3 </h3>--}}
							{{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>--}}
						{{--</a>--}}
					{{--</div>--}}
				{{--</div>--}}

				{{--<div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">--}}
					{{--<div class="card card__blank text-center">--}}
						{{--<a href="#servico-detalhes">--}}
							{{--<div class="card__icone">--}}
								{{--<i class="fas fa-file-alt"></i>--}}
							{{--</div>--}}

							{{--<h3> Serviço 4 </h3>--}}
							{{--<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>--}}
						{{--</a>--}}
					{{--</div>--}}
				{{--</div>--}}
			{{--</div>--}}

		{{--</div>--}}
	{{--</section>--}}
@endsection