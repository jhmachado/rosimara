@extends('site::layouts.master')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                {!!
                    PartialViews::carregarViewParcial([
                        'path' => 'site::partials.imoveis-filtros'
                    ])
                !!}
            </div>

            <divc class="col-lg-9">
                <div class="row">
                    {!!
                        PartialViews::carregarViewParcial([
                            'path' => 'site::partials.section-imoveis',
                            'data' => [
                                'titulo' => '',
                                'linkParaListagem' => '',
                                'imoveis' => $imoveis,
                                'paginacao' => $paginacao,
                                'tamanhoDaColuna' => 'col-xs-12 col-sm-6 col-md-6 col-lg-4 mt-10 mb-5'
                            ]
                        ])
                    !!}
                </div>
            </divc>
        </div>
    </div>

@endsection