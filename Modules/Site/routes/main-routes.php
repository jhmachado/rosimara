<?php

Route::group(['namespace' => 'Modules\Site\Http\Controllers'], function() {
	Route::get('/', 'HomeController@getIndex')->name('site.home');
    Route::get('/imagens/{tipo}/{imagem}', 'ImagensController@getImagem')->name('site.imagens');
    Route::get('/contato', 'ContatoController@getBySlug')->name('site.contato');

    Route::get('/imoveis/{slug?}', 'ImovelController@executarPesquisa')->name('site.imoveis');
    Route::post('/imoveis/{slug?}', 'ImovelController@executarPesquisa')->name('site.imoveis');

    Route::get('/{slug}/{subSlug?}', 'SlugController@getBySlug')->name('site.slug');
});
