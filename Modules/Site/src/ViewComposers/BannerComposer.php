<?php

namespace Modules\Site\ViewComposers;

use Illuminate\View\View;
use Modules\Site\Repositories\Interfaces\BannerInterface;

class BannerComposer
{
	private $bannerRepository;

	public function __construct(BannerInterface $bannerRepository)
	{
		$this->bannerRepository= $bannerRepository;
	}

	public function compose(View $view)
	{
		$banners = $this->bannerRepository->consultarBannersAtivos();
		$view->with('banners', $banners);
	}
}