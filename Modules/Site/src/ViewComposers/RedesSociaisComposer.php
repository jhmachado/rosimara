<?php

namespace Modules\Site\ViewComposers;

use Illuminate\View\View;
use Modules\Site\src\Repositories\Interfaces\RedeSocialInterface;

class RedesSociaisComposer
{
    private $redeSocialRepository;

    public function __construct(RedeSocialInterface $bannerRepository)
    {
        $this->redeSocialRepository= $bannerRepository;
    }

    public function compose(View $view)
    {
        $redesSociais = $this->redeSocialRepository->buscarRedesSociais();
        $view->with('redesSociais', $redesSociais);
    }
}