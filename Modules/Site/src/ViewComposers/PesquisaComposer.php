<?php

namespace Modules\Site\ViewComposers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use Modules\Site\Services\PesquisaService;

class PesquisaComposer
{
    /** @var PesquisaService  */
    private $pesquisaService;

    /** @var array */
    private $input;

    public function __construct(PesquisaService $pesquisaService, Request $request)
    {
        $this->pesquisaService = $pesquisaService;
        $this->input = $request->input();
    }

    public function compose(View $view): void
    {
        $this->registrarOpcoes($view);
        $this->registrarValores($view);
    }

    private function registrarValores(View $view): void
    {
        $filtros = $this->pesquisaService->tratarFiltros($this->input);
        $view->with('filtros', $filtros);
    }

    private function registrarOpcoes(View $view): void
    {
        [$negocios, $tiposDeImoveis, $estados] = $this->pesquisaService->prepararPartial();

        $view->with('tiposDeImoveis', $tiposDeImoveis);
        $view->with('negocios', $negocios);
        $view->with('estados', $estados);
    }
}