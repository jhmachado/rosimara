<?php

namespace Modules\Site\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Collection;
use Modules\Support\Repositories\RepositoryInterface;

interface BannerInterface extends RepositoryInterface
{
    public function consultarBannersAtivos(): ?Collection;
}