<?php

namespace Modules\Site\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Collection;
use Modules\Support\Repositories\RepositoryInterface;

interface CidadeInterface extends RepositoryInterface
{
    public function consultarCidadesComImoveis($uf = ''): ?Collection;
}