<?php

namespace Modules\Site\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Collection;
use Modules\Admin\Models\TipoNegocio;
use Modules\Support\Repositories\RepositoryInterface;

interface TipoNegocioInterface extends RepositoryInterface
{
  public function consultarTiposDeTipoNegocio(): ?Collection;

  public function consultarTipoDeNegocioPorTitulo($titulo): ?TipoNegocio;
}
