<?php

namespace Modules\Site\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Collection;
use Modules\Support\Repositories\RepositoryInterface;

interface TipoImovelInterface extends RepositoryInterface
{
    public function consultarTiposDeImovel(): ?Collection;
}