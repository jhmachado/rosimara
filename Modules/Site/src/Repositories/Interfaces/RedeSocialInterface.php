<?php

namespace Modules\Site\src\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Collection;
use Modules\Support\Repositories\RepositoryInterface;

interface RedeSocialInterface extends RepositoryInterface
{
    public function buscarRedesSociais(): ?Collection;
}