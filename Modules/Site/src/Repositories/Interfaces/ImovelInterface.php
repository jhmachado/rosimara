<?php

namespace Modules\Site\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Collection;
use Modules\Admin\Models\FotoImovel;
use Modules\Admin\Models\Imovel;
use Modules\Admin\Models\TipoNegocio;
use Modules\Support\Repositories\RepositoryInterface;

interface ImovelInterface extends RepositoryInterface
{
    public function consultarImovelPorId(int $id);

    public function consultarImoveisAdicionadosRecentemente(int $limite): ?Collection;

    public function consultarDestaquesPorTipoDeNegocio(TipoNegocio $tipoNegocio, int $limite): ?Collection;

    public function pesquisar(string $slug, array $filtros, int $pagina, int $quantidadePorPagina): array;

    public function consultarFotoPrincipal(Imovel $imovel): ?FotoImovel;
}