<?php

namespace Modules\Site\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Collection;
use Modules\Admin\Models\Banner;
use Modules\Site\Repositories\Interfaces\BannerInterface;
use Modules\Support\Repositories\AbstractRepository;

class BannerEloquent extends AbstractRepository implements BannerInterface
{
    public function consultarBannersAtivos(): ?Collection
    {
        return Banner::orderBy('ordem')->get();
    }
}