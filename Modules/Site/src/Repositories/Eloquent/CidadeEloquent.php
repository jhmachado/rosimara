<?php

namespace Modules\Site\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Modules\Admin\Models\Cidade;
use Modules\Site\Repositories\Interfaces\CidadeInterface;
use Modules\Support\Repositories\AbstractRepository;

class CidadeEloquent extends AbstractRepository implements CidadeInterface
{
    public function consultarCidadesComImoveis($uf = ''): ?Collection
    {
        return Cidade::all();
    }
}