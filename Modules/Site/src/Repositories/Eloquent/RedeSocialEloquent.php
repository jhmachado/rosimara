<?php

namespace Modules\Site\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Collection;
use Modules\Admin\Models\RedeSocial;
use Modules\Site\src\Repositories\Interfaces\RedeSocialInterface;
use Modules\Support\Repositories\AbstractRepository;

class RedeSocialEloquent extends AbstractRepository implements RedeSocialInterface
{

    public function buscarRedesSociais(): ?Collection
    {
        return RedeSocial::where('status', 1)->get();
    }
}