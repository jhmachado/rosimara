<?php

namespace Modules\Site\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Collection;
use Modules\Admin\Models\TipoNegocio;
use Modules\Site\Repositories\Interfaces\TipoNegocioInterface;
use Modules\Support\Repositories\AbstractRepository;

class TipoNegocioEloquent extends AbstractRepository implements TipoNegocioInterface
{
  public function consultarTiposDeTipoNegocio(): ?Collection
  {
    return TipoNegocio::all();
  }

  public function consultarTipoDeNegocioPorTitulo($titulo): ?TipoNegocio
  {
    return TipoNegocio::where('titulo', $titulo)->first();
  }
}
