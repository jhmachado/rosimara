<?php

namespace Modules\Site\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Collection;
use Modules\Admin\Models\TipoImovel;
use Modules\Site\Repositories\Interfaces\TipoImovelInterface;
use Modules\Support\Repositories\AbstractRepository;

class TipoImovelEloquent extends AbstractRepository implements TipoImovelInterface
{
    public function consultarTiposDeImovel(): ?Collection
    {
        return TipoImovel::all();
    }
}