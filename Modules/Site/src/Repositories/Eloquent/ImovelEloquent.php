<?php

namespace Modules\Site\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Modules\Admin\Models\FotoImovel;
use Modules\Admin\Models\Imovel;
use Modules\Admin\Models\TipoNegocio;
use Modules\Site\Repositories\Interfaces\ImovelInterface;
use Modules\Support\Repositories\AbstractRepository;

class ImovelEloquent extends AbstractRepository implements ImovelInterface
{
	public function consultarImoveisAdicionadosRecentemente(int $limite): ?Collection
	{
		return Imovel::where('status', 1)
					->orderBy('created_at', 'desc')
					->limit($limite)->get();
	}

	public function consultarDestaquesPorTipoDeNegocio(TipoNegocio $tipoNegocio, int $limite): ?Collection
	{
		return Imovel::where('status', 1)
						->where('tipo_negocio_id', $tipoNegocio->id)
						->where('destaque', 1)
						->orderBy('created_at', 'desc')
						->limit($limite)
						->get();
	}

	public function pesquisar(string $slug, array $filtros, int $pagina, int $quantidadePorPagina): array
	{
		$query = Imovel::query();
		$this->tratarFiltroDeNegocio($query, $filtros['tipo']);
		$this->tratarFiltroDeTipoDeImovel($query, $filtros['negocio']);
		$this->tratarFiltroDeEndereco($query, $filtros['endereco']);
		$quantidadeEncontrada  = $query->count();

		$pular = $pagina * 9;
		$query->offset($pular)->limit($quantidadePorPagina);
		$imoveisEncontrados = $query->get();

		return [
			'quantidadeEncontrada' => $quantidadeEncontrada,
			'imoveisEncontrados' => $imoveisEncontrados
		];
	}

	private function tratarFiltroDeNegocio(Builder $query, int $negocioId): Builder
	{
		if ($negocioId > 0) {
			$query->where('tipo_negocio_id', $negocioId);
		}

		return $query;
	}

	private function tratarFiltroDeTipoDeImovel(Builder $query, int $tipoImovelId): Builder
	{
		if ($tipoImovelId > 0) {
			$query->where('tipo_imovel_id', $tipoImovelId);
		}

		return $query;
	}

	private function tratarFiltroDeEndereco(Builder $query, ?string $endereco): Builder
	{
		if (!empty($endereco)) {
			$query->where('endereco', 'like', '%' . $endereco . '%');
		}

		return $query;
	}

	public function consultarImovelPorId(int $id): Imovel
	{
		return Imovel::findOrFail($id);
	}

	public function consultarFotoPrincipal(Imovel $imovel): ?FotoImovel
	{
		return $imovel
			->fotos()
			->orderBy('principal', 'desc')
			->orderBy('ordem')
			->first();
	}
}