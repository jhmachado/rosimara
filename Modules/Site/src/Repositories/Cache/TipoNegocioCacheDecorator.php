<?php

namespace Modules\Site\Repositories\Cache;

use Illuminate\Database\Eloquent\Collection;
use Modules\Admin\Models\TipoNegocio;
use Modules\Site\Repositories\Interfaces\TipoNegocioInterface;
use Modules\Support\Cache\CacheInterface;
use Support\Cache\AbstractCacheDecorator;

class TipoNegocioCacheDecorator extends AbstractCacheDecorator implements TipoNegocioInterface
{
    public function __construct(TipoNegocioInterface $repository, CacheInterface $cache)
    {
        $this->repository = $repository;
        $this->cache = $cache;
    }

    public function consultarTiposDeTipoNegocio(): ?Collection
    {
        return $this->getDataIfExistCache(__FUNCTION__, func_get_args());
    }

    public function consultarTipoDeNegocioPorTitulo($titulo): ?TipoNegocio
    {
        return $this->getDataIfExistCache(__FUNCTION__, func_get_args());
    }
}
