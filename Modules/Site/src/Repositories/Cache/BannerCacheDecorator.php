<?php

namespace Modules\Site\Repositories\Cache;

use Illuminate\Database\Eloquent\Collection;
use Modules\Site\Repositories\Interfaces\BannerInterface;
use Modules\Support\Cache\CacheInterface;
use Support\Cache\AbstractCacheDecorator;

class BannerCacheDecorator extends AbstractCacheDecorator implements BannerInterface
{
    protected $repository;

    protected $cache;

    public function __construct(BannerInterface $repository, CacheInterface $cache)
    {
        $this->repository = $repository;
        $this->cache = $cache;
    }

    public function consultarBannersAtivos(): ?Collection
    {
        return $this->getDataIfExistCache(__FUNCTION__, func_get_args());
    }
}