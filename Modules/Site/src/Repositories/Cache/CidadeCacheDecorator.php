<?php

namespace Modules\Site\Repositories\Cache;

use Illuminate\Database\Eloquent\Collection;
use Modules\Site\Repositories\Interfaces\CidadeInterface;
use Modules\Support\Cache\CacheInterface;
use Support\Cache\AbstractCacheDecorator;

class CidadeCacheDecorator extends AbstractCacheDecorator implements CidadeInterface
{
    public function __construct(CidadeInterface $repository, CacheInterface $cache)
    {
        $this->repository = $repository;
        $this->cache = $cache;
    }

    public function consultarCidadesComImoveis($uf = ''): ?Collection
    {
        return $this->getDataIfExistCache(__FUNCTION__, func_get_args());
    }
}