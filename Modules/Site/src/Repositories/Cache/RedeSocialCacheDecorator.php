<?php

namespace Modules\Site\Repositories\Cache;

use Illuminate\Database\Eloquent\Collection;
use Modules\Site\src\Repositories\Interfaces\RedeSocialInterface;
use Modules\Support\Cache\CacheInterface;
use Support\Cache\AbstractCacheDecorator;

class RedeSocialCacheDecorator extends AbstractCacheDecorator implements RedeSocialInterface
{
    public function __construct(RedeSocialInterface $repository, CacheInterface $cache)
    {
        $this->repository = $repository;
        $this->cache = $cache;
    }

    public function buscarRedesSociais(): ?Collection
    {
        return $this->getDataIfExistCache(__FUNCTION__, func_get_args());
    }
}