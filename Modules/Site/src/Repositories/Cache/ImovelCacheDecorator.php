<?php

namespace Modules\Site\Repositories\Cache;

use Illuminate\Database\Eloquent\Collection;
use Modules\Admin\Models\Imovel;
use Modules\Admin\Models\FotoImovel;
use Modules\Admin\Models\Negocio;
use Modules\Site\Repositories\Interfaces\ImovelInterface;
use Modules\Support\Cache\CacheInterface;
use Support\Cache\AbstractCacheDecorator;

class ImovelCacheDecorator extends AbstractCacheDecorator implements ImovelInterface
{
    public function __construct(ImovelInterface $repository, CacheInterface $cache)
    {
        $this->repository = $repository;
        $this->cache = $cache;
    }

    public function consultarImoveisAdicionadosRecentemente(int $limite): Collection
    {
        return $this->getDataIfExistCache(__FUNCTION__, func_get_args());
    }

    public function consultarDestaquesPorNegocio(Negocio $negocio, int $limite): Collection
    {
        return $this->getDataIfExistCache(__FUNCTION__, func_get_args());
    }

    public function consultarImovelPorId(int $id)
    {
        return $this->getDataIfExistCache(__FUNCTION__, func_get_args());
    }

    public function pesquisar(string $slug, array $filtros, int $pagina, int $quantidadePorPagina): array
    {
        return $this->getDataIfExistCache(__FUNCTION__, func_get_args());
    }

    public function consultarFotoPrincipal(Imovel $imovel): ?FotoImovel
    {
        return $this->getDataIfExistCache(__FUNCTION__, func_get_args());
    }
}