<?php

namespace Modules\Site\Repositories\Cache;

use Illuminate\Database\Eloquent\Collection;
use Modules\Site\Repositories\Interfaces\TipoImovelInterface;
use Modules\Support\Cache\CacheInterface;
use Support\Cache\AbstractCacheDecorator;

class TipoImovelCacheDecorator extends AbstractCacheDecorator implements TipoImovelInterface
{
    public function __construct(TipoImovelInterface $repository, CacheInterface $cache)
    {
        $this->repository = $repository;
        $this->cache = $cache;
    }

    public function consultarTiposDeImovel(): ?Collection
    {
        return $this->getDataIfExistCache(__FUNCTION__, func_get_args());
    }
}