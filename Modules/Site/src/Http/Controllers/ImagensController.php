<?php

namespace Modules\Site\Http\Controllers;

class ImagensController extends BaseController
{
    public function getImagem(string $tipo, string $imagem)
    {
        return response()->file(storage_path('app/uploads/'.$tipo.'/'.$imagem));
    }
}