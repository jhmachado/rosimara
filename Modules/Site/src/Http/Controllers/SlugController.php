<?php

namespace Modules\Site\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use Modules\Site\Services\SlugService;

class SlugController extends BaseController
{
    private $classMap = [
        'imovel' => ImovelController::class,
    ];

    private function recuperarControllerResponsavel(string $reference)
    {
        if (isset($this->classMap[$reference])) {
            return $this->classMap[$reference];
        }

        abort(404);
    }

    public function getBySlug(string $slug, Request $request, SlugService $slugService): View
    {
        $slugInstance = $slugService->consultarSlug($slug);
        if ($slugInstance === null) {
            abort(404);
        }

        $controllerResponsavel = $this->recuperarControllerResponsavel($slugInstance->reference);
        return app($controllerResponsavel)->tratarViewPorSlug($request, $slugInstance);
    }
}