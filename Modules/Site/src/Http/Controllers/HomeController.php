<?php

namespace Modules\Site\Http\Controllers;

use Modules\Site\Services\ImovelService;

class HomeController extends BaseController
{
	public function getIndex(ImovelService $imoveisService)
	{
		[$adicionadosRecentemente, $destaquesDeVenda, $destaquesDeLocacao] = $imoveisService->consultarImoveisParaHome();

		return view('site::pages.home', [
			'adicionadosRecentemente' => $adicionadosRecentemente,
			'destaquesDeVenda' => $destaquesDeVenda,
			'destaquesDeLocacao' => $destaquesDeLocacao,
		]);
	}
}