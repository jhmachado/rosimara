<?php

namespace Modules\Site\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Modules\Site\Services\PesquisaService;

class BaseController extends Controller
{
    /** @var PesquisaService */
    private $pesquisaService;

    private const QUANTIDADE_POR_PAGINA = 9;

    public function __construct(PesquisaService $imovelService)
    {
        $this->pesquisaService = $imovelService;
    }

    public function aplicarSEO()
    {

    }

    public function exibirPaginaDePesquisa(Request $request, string $slug = ''): View
    {
        $input = $request->input();
        $paginaAtual = intval($request->pagina);
        $filtros = $this->pesquisaService->tratarFiltros($input);

        $retornoDaPesquisa = $this->pesquisaService->pesquisar(
            $slug,
            $filtros,
            $paginaAtual,
            self::QUANTIDADE_POR_PAGINA
        );

        $paginacao = $this->pesquisaService->tratarPaginacao(
            $paginaAtual,
            $retornoDaPesquisa['quantidadeEncontrada'],
            self::QUANTIDADE_POR_PAGINA
        );

        return view('site::pages.lista-imoveis', [
            'imoveis' => $retornoDaPesquisa['imoveisEncontrados'],
            'paginacao' => $paginacao,
        ]);
	}
}