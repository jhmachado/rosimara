<?php

namespace Modules\Site\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\View\View;
use Modules\Admin\Models\Slug;
use Modules\Site\Services\ImovelService;

class ImovelController extends BaseController
{
    public function tratarViewPorSlug(Request $request, Slug $slug): View
    {
        try {

            $imovelService = ImovelService::build();
            $imovel = $imovelService->consultarImovelPorId($slug->reference_id);

            return view('site::pages.imovel-interna', [
                'imovel' => $imovel,
            ]);

        } catch (\Exception $e) {
            dd($e);
            return abort(404);
        }
    }
}