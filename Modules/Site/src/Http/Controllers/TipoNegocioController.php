<?php

namespace Modules\Site\Http\Controllers;

use Illuminate\Http\Request;

class TipoNegocioController extends BaseController
{
    public function tratarViewPorSlug(Request $request, string $slug = '')
    {
        return $this->exibirPaginaDePesquisa($request, $slug);
    }

}
