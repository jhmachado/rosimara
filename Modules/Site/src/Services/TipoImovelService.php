<?php

namespace Modules\Site\Services;

use Illuminate\Database\Eloquent\Collection;
use Modules\Site\Repositories\Interfaces\TipoImovelInterface;

class TipoImovelService
{
    private $repository;

    public function __construct(TipoImovelInterface $repository)
    {
        $this->repository = $repository;
    }

    public function consultarTiposDeImovel(): Collection
    {
        return $this->repository->consultarTiposDeImovel();
    }
}