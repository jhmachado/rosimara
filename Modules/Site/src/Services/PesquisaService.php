<?php

namespace Modules\Site\Services;

use Modules\Site\Repositories\Interfaces\ImovelInterface;

class PesquisaService
{
    /** @var TipoNegocioService  */
    private $tipoNegocioService;

    /** @var TipoImovelService  */
    private $tipoImovelService;

    /** @var ImovelInterface  */
    private $imovelRepository;

    public function __construct(
        TipoNegocioService $tipoNegocioService,
        TipoImovelService $tipoImovelService,
        ImovelInterface $imovelRepository
    )
    {
        $this->tipoNegocioService = $tipoNegocioService;
        $this->tipoImovelService = $tipoImovelService;
        $this->imovelRepository = $imovelRepository;
    }

    public function prepararPartial()
    {
        $negocios = $this->tipoNegocioService->consultarTiposDeNegocio();
        $tiposDeImoveis = $this->tipoImovelService->consultarTiposDeImovel();
        $estados = get_estados();

        return [$negocios, $tiposDeImoveis, $estados];
    }

    public function pesquisar(string $slug, array $filtros, int $pagina, int $quantidadePorPagina): array
    {
        return $this->imovelRepository->pesquisar($slug, $filtros, $pagina, $quantidadePorPagina);
    }

    public function tratarFiltros(array $input): array
    {
        $tipo = array_get($input, 'tipo', 0);
        $negocio = array_get($input, 'negocio', 0);
        $endereco = array_get($input, 'endereco', '');
        $estado = array_get($input, 'estado', '');

        return [
            'tipo' => $tipo,
            'negocio' => $negocio,
            'endereco' => $endereco,
            'estado' => $estado,
        ];
    }

    public function tratarPaginacao(int $paginaAtual, int $quantidadeEncontrado, int $quantidadePorPagina): array
    {
        $exibirLinkParaPrimeiraPagina = ($paginaAtual > 0);
        $exibirLinkParaUltimaPagina = ($paginaAtual < ceil($quantidadeEncontrado / $quantidadePorPagina));
        $primeiroLink = $this->calcularPrimeiroLink($paginaAtual);

        $maximoDePaginas = ceil($quantidadeEncontrado / $quantidadePorPagina);
        $ultimoLink = $this->calcularUltimoLink($primeiroLink, $maximoDePaginas);

        return [
            'exibirLinkParaPrimeiraPagina' => $exibirLinkParaPrimeiraPagina,
            'exibirLinkParaUltimaPagina' => $exibirLinkParaUltimaPagina,
            'primeiroLink' => $primeiroLink,
            'ultimoLink' => $ultimoLink,
            'maximoDePaginas' => $maximoDePaginas,
        ];
    }

    private function calcularPrimeiroLink(int $paginaAtual): int
    {
        $primeiroLink = ($paginaAtual - 3);
        return ($primeiroLink < 0) ? 1 : $primeiroLink;
    }

    private function calcularUltimoLink(int $primeiroLink, $maximoDePaginas)
    {
        $ultimoLink = $primeiroLink + 2;
        return ($ultimoLink > $maximoDePaginas) ? $maximoDePaginas : $ultimoLink;
    }

    private function tratarSlug($slug)
    {

    }
}
