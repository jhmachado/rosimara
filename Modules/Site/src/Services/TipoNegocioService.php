<?php

namespace Modules\Site\Services;

use Modules\Admin\Models\TipoNegocio;
use Modules\Site\Repositories\Interfaces\TipoNegocioInterface;

class TipoNegocioService
{
    private $repository;

    public function __construct(TipoNegocioInterface $repository)
    {
        $this->repository = $repository;
    }

    public function consultarTiposDeNegocio()
    {
        return $this->repository->consultarTiposDeNegocio();
    }

    public function consultarTipoDeNegocioPorTitulo($titulo): TipoNegocio
    {
        return $this->repository->consultarTipoDeNegocioPorTitulo($titulo);
    }
}
