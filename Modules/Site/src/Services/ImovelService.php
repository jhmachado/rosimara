<?php

namespace Modules\Site\Services;

use Illuminate\Database\Eloquent\Collection;
use Modules\Site\Repositories\Interfaces\ImovelInterface;

class ImovelService
{
	/** @var ImovelInterface */
	private $imovelRepository;
	/** @var TipoNegocioService */
	private $tipoNegocioService;

	public function __construct(ImovelInterface $imovelRepository, TipoNegocioService $tipoNegocioService)
	{
		$this->imovelRepository = $imovelRepository;
		$this->tipoNegocioService = $tipoNegocioService;
	}

	public function consultarImovelPorId(int $id)
	{
		return $this->imovelRepository->consultarImovelPorId($id);
	}

	public function consultarImoveisParaHome()
	{
		$adicionadosRecentemente = $this->consultarImoveisAdicionadosRecentemente(4);
		$destaquesDeVenda = $this->consultarDestaquesPorTipoNegocio('Venda', 4);
		$destaquesDeLocacao = $this->consultarDestaquesPorTipoNegocio('Locação', 4);

		return [$adicionadosRecentemente, $destaquesDeVenda, $destaquesDeLocacao];
	}

	public function consultarImoveisAdicionadosRecentemente($limite): Collection
	{
		return $this->imovelRepository->consultarImoveisAdicionadosRecentemente($limite);
	}

	public function consultarDestaquesPorTipoNegocio($tituloDoTipoNegocio, $limite)
	{
		$tipoNegocio = $this->tipoNegocioService->consultarTipoDeNegocioPorTitulo($tituloDoTipoNegocio);
		return $this->imovelRepository->consultarDestaquesPorTipoDeNegocio($tipoNegocio, $limite);
	}
}