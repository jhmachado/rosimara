<?php

namespace Modules\Site\Services;

use Modules\Admin\Models\Slug;
use Modules\Admin\Repositories\Interfaces\SlugInterface;

class SlugService
{
    public function __construct(SlugInterface $slugRepository)
    {
        $this->repository = $slugRepository;
    }

    public function consultarSlug($slug): ?Slug
    {
        return $this->repository->consultarSlug($slug);
    }
}