<?php

namespace Modules\Site\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Admin\Models\Banner;
use Modules\Admin\Models\Cidade;
use Modules\Admin\Models\Imovel;
use Modules\Admin\Models\TipoNegocio;
use Modules\Admin\Models\RedeSocial;
use Modules\Admin\Models\TipoImovel;
use Modules\Site\Repositories\Cache\BannerCacheDecorator;
use Modules\Site\Repositories\Cache\CidadeCacheDecorator;
use Modules\Site\Repositories\Cache\ImovelCacheDecorator;
use Modules\Site\Repositories\Cache\TipoNegocioCacheDecorator;
use Modules\Site\Repositories\Cache\RedeSocialCacheDecorator;
use Modules\Site\Repositories\Cache\TipoImovelCacheDecorator;
use Modules\Site\Repositories\CidadeEloquent;
use Modules\Site\Repositories\Eloquent\BannerEloquent;
use Modules\Site\Repositories\Eloquent\ImovelEloquent;
use Modules\Site\Repositories\Eloquent\TipoNegocioEloquent;
use Modules\Site\Repositories\Eloquent\RedeSocialEloquent;
use Modules\Site\Repositories\Eloquent\TipoImovelEloquent;
use Modules\Site\Repositories\Interfaces\BannerInterface;
use Modules\Site\Repositories\Interfaces\CidadeInterface;
use Modules\Site\Repositories\Interfaces\ImovelInterface;
use Modules\Site\Repositories\Interfaces\TipoNegocioInterface;
use Modules\Site\Repositories\Interfaces\TipoImovelInterface;
use Modules\Site\src\Repositories\Interfaces\RedeSocialInterface;
use Modules\Support\Cache\Cache;

class SingletonServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        if (setting('enable_cache', false)) {
            $this->registrarComCache();
        } else {
            $this->registrarSemCache();
        }
    }

    private function registrarComCache(): void
    {
        $this->app->singleton(BannerInterface::class, function () {
            return new BannerCacheDecorator(new BannerEloquent(new Banner()), new Cache($this->app['cache'], BannerEloquent::class));
        });

        $this->app->singleton(CidadeInterface::class, function () {
            return new CidadeCacheDecorator(new CidadeEloquent(new Cidade()), new Cache($this->app['cache'], CidadeEloquent::class));
        });

        $this->app->singleton(TipoImovelInterface::class, function () {
            return new TipoImovelCacheDecorator(new TipoImovelEloquent(new TipoImovel()), new Cache($this->app['cache'], TipoImovelEloquent::class));
        });

        $this->app->singleton(TipoNegocioInterface::class, function () {
            return new TipoNegocioCacheDecorator(new TipoNegocioEloquent(new TipoNegocio()), new Cache($this->app['cache'], TipoNegocioEloquent::class));
        });

        $this->app->singleton(ImovelInterface::class, function () {
            return new ImovelCacheDecorator(new ImovelEloquent(new Imovel()), new Cache($this->app['cache'], ImovelEloquent::class));
        });

        $this->app->singleton(RedeSocialInterface::class, function () {
            return new RedeSocialCacheDecorator(new RedeSocialEloquent(new RedeSocial()), new Cache($this->app['cache'], RedeSocialEloquent::class));
        });
    }

    private function registrarSemCache(): void
    {
        $this->app->singleton(BannerInterface::class, function () {
            return new BannerEloquent(new Banner());
        });

        $this->app->singleton(CidadeInterface::class, function () {
            return new CidadeEloquent(new Cidade());
        });

        $this->app->singleton(TipoImovelInterface::class, function () {
            return new TipoImovelEloquent(new TipoImovel());
        });

        $this->app->singleton(TipoNegocioInterface::class, function () {
            return new TipoNegocioEloquent(new TipoNegocio());
        });

        $this->app->singleton(ImovelInterface::class, function () {
            return new ImovelEloquent(new Imovel());
        });

        $this->app->singleton(RedeSocialInterface::class, function () {
            return new RedeSocialEloquent(new RedeSocial());
        });
    }
}
