<?php

namespace Modules\Site\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Modules\Site\ViewComposers\BannerComposer;
use Modules\Site\ViewComposers\PesquisaComposer;
use Modules\Site\ViewComposers\RedesSociaisComposer;

class ComposerServiceProvider extends ServiceProvider
{
    public function boot()
    {
        View::composer('site::partials.banner', BannerComposer::class);
        View::composer('site::partials.imoveis-filtros', PesquisaComposer::class);
        View::composer('site::partials.redes-sociais', RedesSociaisComposer::class);
    }
}