<?php

namespace Modules\Site\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Support\src\Supports\Helper;

class SiteServiceProvider extends ServiceProvider
{
	public function register()
	{
		$this->loadViewsFrom(__DIR__ . '/../../resources/views', 'site');
	}

	public function boot()
    {
        $this->app->register(RouteServiceProvider::class);
        $this->app->register(SingletonServiceProvider::class);
        $this->app->register(ComposerServiceProvider::class);
        Helper::autoload(__DIR__ . '/../../helpers');
    }
}
