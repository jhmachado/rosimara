<?php

namespace Modules\Site\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
	public function boot()
	{
		$this->loadRoutesFrom(base_path('Modules/Site/routes/main-routes.php'));
	}
}
