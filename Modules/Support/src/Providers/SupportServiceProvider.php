<?php

namespace Modules\Support\Providers;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use Modules\Support\src\Supports\Helper;
use Modules\Support\Partials\PartialViews;
use Modules\Support\Supports\Constantes;

class SupportServiceProvider extends ServiceProvider
{
	protected $app;

	public function register()
	{
		Helper::autoload(__DIR__ . '/../../helpers');
		$loader = AliasLoader::getInstance();
		$loader->alias('PartialViews', PartialViews::class);
		$loader->alias('Constantes', Constantes::class);
	}

	public function boot(){}
}
