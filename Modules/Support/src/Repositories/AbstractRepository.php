<?php

namespace Modules\Support\Repositories;

use Illuminate\Database\Eloquent\Model;

class AbstractRepository
{
    private $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function getModel(): Model
    {
        return $this->model;
    }
}