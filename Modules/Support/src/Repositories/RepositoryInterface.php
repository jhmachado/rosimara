<?php

namespace Modules\Support\Repositories;

interface RepositoryInterface
{
    public function getModel();
}