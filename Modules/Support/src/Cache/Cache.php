<?php

namespace Modules\Support\Cache;

use Illuminate\Cache\CacheManager;
use Illuminate\Support\Facades\File;

class Cache implements CacheInterface
{
    protected $cache;

    protected $config;

    public $cacheGroup;

    public function __construct(CacheManager $cache, $cacheGroup, $config = [])
    {
        $this->cache = $cache;
        $this->cacheGroup = $cacheGroup;

        $this->config = !empty($config) ? $config : [
            'cache_time' => 10,
            'stored_keys' => storage_path('cache_keys.json'),
        ];
    }

    public function generateCacheKey($key)
    {
        return md5($this->cacheGroup) . $key;
    }

    public function get($key)
    {
        if (!file_exists($this->config['stored_keys'])) {
            return null;
        }
        return $this->cache->get($this->generateCacheKey($key));
    }

    public function put($key, $value, $minutes = false)
    {
        if (!$minutes) {
            $minutes = $this->config['cache_time'];
        }

        $key = $this->generateCacheKey($key);
        $this->storeCacheKey($key);

        return $this->cache->put($key, $value, $minutes);
    }

    public function has($key)
    {
        if (!file_exists($this->config['stored_keys'])) {
            return false;
        }
        $key = $this->generateCacheKey($key);
        return $this->cache->has($key);
    }

    public function storeCacheKey($key)
    {
        if (file_exists($this->config['stored_keys'])) {
            $cacheKeys = get_file_data($this->config['stored_keys']);
            if (!empty($cacheKeys) && !in_array($key, array_get($cacheKeys, $this->cacheGroup, []))) {
                $cacheKeys[$this->cacheGroup][] = $key;
            }
        } else {
            $cacheKeys = [];
            $cacheKeys[$this->cacheGroup][] = $key;
        }
        save_file_data($this->config['stored_keys'], $cacheKeys);
    }

    public function flush()
    {
        $cacheKeys = [];
        if (file_exists($this->config['stored_keys'])) {
            $cacheKeys = get_file_data($this->config['stored_keys']);
        }
        if (!empty($cacheKeys)) {
            $caches = array_get($cacheKeys, $this->cacheGroup);
            if ($caches) {
                foreach ($caches as $cache) {
                    $this->cache->forget($cache);
                }
                unset($cacheKeys[$this->cacheGroup]);
            }
        }
        if (!empty($cacheKeys)) {
            save_file_data($this->config['stored_keys'], $cacheKeys);
        } else {
            File::delete($this->config['stored_keys']);
        }
    }
}
