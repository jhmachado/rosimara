<?php

namespace Modules\Support\Cache;

interface CacheInterface
{
    public function get($key);

    public function put($key, $value, $minutes = null);

    public function has($key);

    public function flush();
}
