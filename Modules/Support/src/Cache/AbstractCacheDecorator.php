<?php

namespace Support\Cache;

use Modules\Support\Repositories\RepositoryInterface;

abstract class AbstractCacheDecorator implements RepositoryInterface
{
    protected $repository;

    protected $cache;

    public function getCacheInstance()
    {
        return $this->cache;
    }

    public function getModel()
    {
        return $this->repository->getModel();
    }

    public function make(array $with = [])
    {
        return $this->repository->make($with);
    }

    public function getDataIfExistCache($function, array $args)
    {
        try {
            $cacheKey = md5(get_class($this) . $function . serialize(request()->input()) . serialize(func_get_args()));

            if ($this->cache->has($cacheKey)) {
                return $this->cache->get($cacheKey);
            }

            $cacheData = call_user_func_array([$this->repository, $function], $args);
            $this->cache->put($cacheKey, $cacheData);
            return $cacheData;

        } catch (\Exception $ex) {
            info($ex->getMessage());
            return call_user_func_array([$this->repository, $function], $args);
        }
    }

    public function getDataWithoutCache($function, array $args)
    {
        return call_user_func_array([$this->repository, $function], $args);
    }

    public function flushCacheAndUpdateData($function, $args, $flushCache = true)
    {
        if ($flushCache) {
            $this->cache->flush();
        }

        return call_user_func_array([$this->repository, $function], $args);
    }
}
