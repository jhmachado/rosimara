<?php

namespace Modules\Support\Partials;

class PartialViews
{
	public static function carregarViewParcial($args = []): string
	{
		try {

			$path = array_get($args, 'path');
			$data = array_get($args, 'data', []);

			return view($path, $data)->render();

		} catch (\Throwable $e) {
			return "";
		}
	}
}