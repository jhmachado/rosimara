<?php

namespace Modules\Support\Supports;

class Constantes
{
	/** @var int */
	private const LIMITE_DE_REGISTROS_POR_PAGINA_ADMIN = 10;

	public static function getLimitePorPaginaAdmin()
	{
		return self::LIMITE_DE_REGISTROS_POR_PAGINA_ADMIN;
	}
}