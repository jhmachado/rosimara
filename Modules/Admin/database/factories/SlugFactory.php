<?php

use Faker\Generator as Faker;
use Modules\Admin\Models\Slug;

$factory->define(Slug::class, function (Faker $faker) {
	return [
		'key' => str_slug($faker->name),
		'reference' => $faker->name,
		'reference_id' => $faker->numberBetween(1, 100000),
		'status' => 1,
	];
});