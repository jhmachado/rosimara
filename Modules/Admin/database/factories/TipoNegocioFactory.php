<?php

use Faker\Generator as Faker;
use Modules\Admin\Models\TipoNegocio;

$factory->define(TipoNegocio::class, function (Faker $faker) {
	return [
		'titulo' => $faker->name,
		'status' => 1,
	];
});