<?php

namespace Modules\Admin\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Admin\Models\Foto;
use Modules\Admin\Models\FotoImovel;
use Modules\Admin\Models\Imovel;

class FotoImovelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->fotos = Foto::all();
        $imovels = Imovel::orderBy('id')->limit(15)->get();

        foreach ($imovels as $imovel) {
            $this->relacionarImovelAsFotos($imovel);
        }
    }

    private function relacionarImovelAsFotos(Imovel $imovel): void
    {
        foreach ($this->fotos as $key => $foto) {
            FotoImovel::create([
                'imovel_id' => $imovel->id,
                'foto_id' => $foto->id,
                'principal' => ($key == 0),
                'ordem' => $key,
            ]);
        }
    }
}
