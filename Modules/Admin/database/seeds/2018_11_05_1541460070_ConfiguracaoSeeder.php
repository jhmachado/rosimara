<?php

namespace Modules\Admin\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Admin\Models\Configuracao;

class ConfiguracaoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Configuracao::create([
            'key' => 'logo',
            'value' => url('/logo.png'),
        ]);

        Configuracao::create([
            'key' => 'titulo_base',
            'value' => 'Rosimara',
        ]);
        
        Configuracao::create([
            'key' => 'email',
            'value' => 'contato@rosimara.com.br',
        ]);

        Configuracao::create([
            'key' => 'telefone',
            'value' => '(44) 99999 - 9999',
        ]);

        Configuracao::create([
            'key' => 'sobre-mim',
            'value' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        ]);

        Configuracao::create([
            'key' => 'enable_cache',
            'value' => 1
        ]);
    }
}
