<?php

namespace Modules\Admin\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Admin\Models\Menu;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->generateHeaderMenu();
        $this->generateFooterMenu();
    }

    public function generateHeaderMenu()
    {
    	$menuCabecalho = Menu::create([
    		'titulo' => 'Cabeçalho',
    		'slug' => 'cabecalho',
    	]);

    	$menuCabecalho->itens()->create([
    		'titulo' => 'HOME',
    		'url' => url('/'),
    		'target' => '',
    	]);
    	
    	$menuCabecalho->itens()->create([
    		'titulo' => 'VENDA',
    		'url' => url('/venda'),
    		'target' => '',
    	]);
    	
    	$menuCabecalho->itens()->create([
    		'titulo' => 'LOCAÇÃO',
    		'url' => url('/locacao'),
    		'target' => '',
    	]);
    	
    	$menuCabecalho->itens()->create([
    		'titulo' => 'LANÇAMENTOS',
    		'url' => url('/lancamentos'),
    		'target' => '',
    	]);
    	
    	$menuCabecalho->itens()->create([
    		'titulo' => 'CONTATO',
    		'url' => url('/contato'),
    		'target' => '',
    	]);
    }

    public function generateFooterMenu()
    {
    	$menuCabecalho = Menu::create([
    		'titulo' => 'Rodapé',
    		'slug' => 'rodape',
    	]);

    	$menuCabecalho->itens()->create([
    		'titulo' => 'Seja um cliente',
    		'url' => url('/'),
    		'target' => '',
    	]);
    	
    	$menuCabecalho->itens()->create([
    		'titulo' => 'Entre em contato',
    		'url' => url('/contato'),
    		'target' => '',
    	]);
    	
    	$menuCabecalho->itens()->create([
    		'titulo' => 'Dúvidas frequentes',
    		'url' => url('/duvidas-frequentes'),
    		'target' => '',
    	]);
    }
}
