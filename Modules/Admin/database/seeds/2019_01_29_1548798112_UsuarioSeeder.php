<?php

namespace Modules\Admin\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Admin\Models\Usuario;

class UsuarioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$senha = bcrypt('admin');

        Usuario::create([
        	'nome' => 'Administrador',
        	'login' => 'admin',
        	'senha' => $senha,
        ]);
    }
}
