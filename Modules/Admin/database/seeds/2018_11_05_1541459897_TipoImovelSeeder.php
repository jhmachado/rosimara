<?php

namespace Modules\Admin\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Admin\Models\TipoImovel;
use Modules\Admin\Models\Slug;

class TipoImovelSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $apartamento = TipoImovel::updateOrCreate(
        	['titulo' => 'Apartamento'],
        	['status' => 1]
        );
        
        Slug::updateOrCreate(
        	[
        		'reference_id' => $apartamento->id,
        		'key' => 'apartamento',
        	],
        	['reference' => 'tipoimovel']
        );

        $casa = TipoImovel::updateOrCreate(
        	['titulo' => 'Casa'],
        	['status' => 1]
        );

        Slug::updateOrCreate(
        	[
        		'reference_id' => $casa->id,
        		'key' => 'casa',
        	],
        	['reference' => 'tipoimovel']
        );
    }
}
