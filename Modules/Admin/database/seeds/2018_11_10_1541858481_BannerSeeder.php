<?php

namespace Modules\Admin\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Admin\Models\Banner;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Banner::create([
        	'titulo' => 'Banner 1',
        	'url' => 'http://rosimara.site/',
        	'target' => '_blank',
        	'imagem' => 'banner-2.jpg',
        	'texto' => '',
        	'ordem' => 2,
        ]);
        
        Banner::create([
        	'titulo' => 'Banner 1',
        	'url' => 'http://rosimara.site/',
        	'target' => '_blank',
            'imagem' => 'banner-1.jpg',
            'texto' => 'teste',
        	'ordem' => 2,
        ]);
    }
}
