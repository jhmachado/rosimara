<?php

namespace Modules\Admin\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Admin\Models\Foto;

class FotoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Foto::create([
        	'titulo' => 'Frente imóvel',
        	'url' => url('/assets/imagens/imovel-1.jpg'),
        	'tamanho' => 0,
        ]);
        
        Foto::create([
        	'titulo' => 'Frente imóvel',
        	'url' => url('/assets/imagens/imovel-2.jpg'),
        	'tamanho' => 0,
        ]);
        
        Foto::create([
        	'titulo' => 'Frente imóvel',
        	'url' => url('/assets/imagens/imovel-3.jpg'),
        	'tamanho' => 0,
        ]);
        
        Foto::create([
        	'titulo' => 'Frente imóvel',
        	'url' => url('/assets/imagens/imovel-4.jpg'),
        	'tamanho' => 0,
        ]);
        
        Foto::create([
        	'titulo' => 'Frente imóvel',
        	'url' => url('/assets/imagens/imovel-5.jpg'),
        	'tamanho' => 0,
        ]);
        
        Foto::create([
        	'titulo' => 'Frente imóvel',
        	'url' => url('/assets/imagens/imovel-6.jpg'),
        	'tamanho' => 0,
        ]);
        
        Foto::create([
        	'titulo' => 'Frente imóvel',
        	'url' => url('/assets/imagens/imovel-7.jpg'),
        	'tamanho' => 0,
        ]);
        
        Foto::create([
        	'titulo' => 'Frente imóvel',
        	'url' => url('/assets/imagens/imovel-8.jpg'),
        	'tamanho' => 0,
        ]);

    }
}
