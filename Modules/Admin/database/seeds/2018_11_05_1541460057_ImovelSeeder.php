<?php

namespace Modules\Admin\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Admin\Models\TipoNegocio;
use Modules\Admin\Models\TipoImovel;
use Modules\Admin\Models\Imovel;
use Modules\Admin\Models\Foto;
use Modules\Admin\Models\Cidade;
use Modules\Admin\Models\Bairro;

class ImovelSeeder extends Seeder
{
    private $locacao;
    private $venda;
    private $casa;
    private $apartamento;
    private $cidade;
    private $bairro;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->locacao = TipoNegocio::where('titulo', 'Locação')->first();
    	$this->venda = TipoNegocio::where('titulo', 'Venda')->first();

    	$this->apartamento = TipoImovel::where('titulo', 'Apartamento')->first();
        $this->casa = TipoImovel::where('titulo', 'Casa')->first();
        
        $this->cidade = Cidade::first();
    	$this->bairro = Bairro::first();

    	$this->inserirImoveisParaLocacao();
    	$this->inserirImoveisParaVenda();
    }

    public function inserirImoveisParaLocacao()
    {
	    for ($i = 0; $i < 15; $i++) { 
    		Imovel::create([
    			'tipo_negocio_id' => $this->locacao->id,
    			'tipo_imovel_id' => $this->apartamento->id,
                'cidade_id' => $this->cidade->id,
                'bairro_id' => $this->bairro->id,
                'titulo' => 'Imóvel 000',
                'endereco' => 'Rua não sei esquina com talvez',
                'preco' => '',
                'descricao' => '',
    		]);
        }

        for ($i = 0; $i < 15; $i++) { 
    		Imovel::create([
    			'tipo_negocio_id' => $this->venda->id,
    			'tipo_imovel_id' => $this->casa->id,
                'cidade_id' => $this->cidade->id,
                'bairro_id' => $this->bairro->id,
                'titulo' => 'Imóvel 000',
                'endereco' => 'Rua não sei esquina com talvez',
                'preco' => '',
                'descricao' => '',
    		]);
        }
    }

    public function inserirImoveisParaVenda()
    {
	    for ($i = 0; $i < 15; $i++) {
    		Imovel::create([
    			'tipo_negocio_id' => $this->venda->id,
    			'tipo_imovel_id' => $this->apartamento->id,
                'cidade_id' => $this->cidade->id,
                'bairro_id' => $this->bairro->id,
                'titulo' => 'Imóvel 000',
                'endereco' => 'Rua não sei esquina com talvez',
                'preco' => '',
                'descricao' => '',
    		]);
        }

        for ($i = 0; $i < 15; $i++) { 
    		Imovel::create([
    			'tipo_negocio_id' => $this->venda->id,
    			'tipo_imovel_id' => $this->casa->id,
                'cidade_id' => $this->cidade->id,
                'bairro_id' => $this->bairro->id,
                'titulo' => 'Imóvel 000',
                'endereco' => 'Rua não sei esquina com talvez',
                'preco' => '',
                'descricao' => '',
    		]);
        }
    }
}
