<?php

namespace Modules\Admin\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Admin\Models\TipoNegocio;
use Modules\Admin\Models\Slug;

class TipoNegocioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $locacao = TipoNegocio::updateOrCreate(
        	['titulo' => 'Locação'],
        	['status' => 1]
        );
        
        Slug::updateOrCreate(
        	[
        		'reference_id' => $locacao->id,
        		'key' => 'locacao',
        	],
        	['reference' => 'tipo_negocio']
        );

        $venda = TipoNegocio::updateOrCreate(
        	['titulo' => 'Venda'],
        	['status' => 1]
        );

        Slug::updateOrCreate(
        	[
        		'reference_id' => $venda->id,
        		'key' => 'venda',
        	],
        	['reference' => 'tipo_negocio']
        );
    }
}
