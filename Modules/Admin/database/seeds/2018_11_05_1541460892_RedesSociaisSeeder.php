<?php

namespace Modules\Admin\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Admin\Models\RedeSocial;

class RedesSociaisSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RedeSocial::create([
        	'icone' => 'facebook',
        	'url' => 'www.facebook.com.br',
        ]);
        
        RedeSocial::create([
        	'icone' => 'twitter',
        	'url' => 'www.twitter.com.br',
        ]);
        
        RedeSocial::create([
        	'icone' => 'linkedin',
        	'url' => 'www.linkedin.com.br',
        ]);
    }
}
