<?php

namespace Modules\Admin\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Admin\Models\Pagina;
use Modules\Admin\Models\Slug;

class PaginaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Pagina::create([
        	'titulo' => 'Quem sou',
        	'conteudo' => '',
        ]);
        
        Pagina::create([
        	'titulo' => 'Minha carreira',
        	'conteudo' => '',
        ]);

    }
}
