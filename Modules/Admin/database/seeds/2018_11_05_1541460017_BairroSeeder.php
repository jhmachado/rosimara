<?php

namespace Modules\Admin\Database\Seeds;

use Illuminate\Database\Seeder;
use Modules\Admin\Models\Cidade;
use Modules\Admin\Models\Bairro;

class BairroSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cidade = Cidade::first();

        for ($i = 0; $i < 10; $i++) { 
        	Bairro::create([
        		'nome' => 'Bairro - ' . $i,
        		'cidade_id' => $cidade->id,
        	]);
        }
    }
}
