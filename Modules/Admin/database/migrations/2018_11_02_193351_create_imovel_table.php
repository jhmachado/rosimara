<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImovelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imovel', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tipo_negocio_id')->unsigned();
            $table->integer('tipo_imovel_id')->unsigned();
            $table->integer('cidade_id')->unsigned();
            $table->integer('bairro_id')->unsigned();

            $table->string('titulo', 255);
            $table->string('endereco', 255);
            $table->string('preco', 255);
            $table->text('descricao');
	        $table->tinyInteger('status')
                    ->unsigned()
                    ->default(1);

	        $table->boolean('destaque')
		        ->default(0);

	        $table->timestamps();
            $table->softDeletes();
            $table->engine = 'InnoDB';

            $table->foreign('tipo_negocio_id')
                    ->references('id')
                    ->on('tipo_negocio')
                    ->onUpdate('no action')
                    ->onDelete('no action');

            $table->foreign('tipo_imovel_id')
                    ->references('id')
                    ->on('tipo_imovel')
                    ->onUpdate('no action')
                    ->onDelete('no action');

            $table->foreign('cidade_id')
                    ->references('id')
                    ->on('cidade')
                    ->onUpdate('no action')
                    ->onDelete('no action');

            $table->foreign('bairro_id')
                    ->references('id')
                    ->on('bairro')
                    ->onUpdate('no action')
                    ->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imovel');
    }
}
