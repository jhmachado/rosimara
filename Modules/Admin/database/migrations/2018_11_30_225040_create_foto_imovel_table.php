<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFotoImovelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('foto_imovel', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('imovel_id')->unsigned();
            $table->integer('foto_id')->unsigned();
            $table->integer('ordem');
            $table->boolean('principal');

            $table->tinyInteger('status')
                ->unsigned()
                ->default(1);

            $table->timestamps();
            $table->softDeletes();
            $table->engine = 'InnoDB';

            $table->foreign('foto_id')
                ->references('id')
                ->on('foto')
                ->onUpdate('no action')
                ->onDelete('no action');

            $table->foreign('imovel_id')
                ->references('id')
                ->on('imovel')
                ->onUpdate('no action')
                ->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('foto_imovel');
    }
}
