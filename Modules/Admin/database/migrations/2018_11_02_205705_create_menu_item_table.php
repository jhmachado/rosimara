<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menu_item', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_id')->unsigned();

            $table->string('titulo', 255);
            $table->string('url', 255);
            $table->string('target', 20)->default('_self');
            
            $table->tinyInteger('status')
                    ->unsigned()
                    ->default(1);

            $table->timestamps();
            $table->softDeletes();
            $table->engine = 'InnoDB';

            $table->foreign('menu_id')
                    ->references('id')
                    ->on('menu')
                    ->onUpdate('no action')
                    ->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menu_item');
    }
}
