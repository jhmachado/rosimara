<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImovelDetalheTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imovel_detalhe', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('imovel_id')->unsigned();

            $table->string('titulo', 255);
            $table->string('valor', 255);
            $table->string('icone', 255);
            $table->integer('ordem');

            $table->tinyInteger('status')
                    ->unsigned()
                    ->default(1);

            $table->timestamps();
            $table->softDeletes();
            $table->engine = 'InnoDB';

            $table->foreign('imovel_id')
                    ->references('id')
                    ->on('imovel')
                    ->onUpdate('no action')
                    ->onDelete('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imovel_detalhe');
    }
}
