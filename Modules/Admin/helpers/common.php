<?php

use Modules\Admin\Models\Configuracao;

if (!function_exists('setting')) {
    function setting(string $key = null, $default = null)
    {
        try {

            return Configuracao::where('key', [$key, $default])->first();

        } catch (Throwable $e) {
            return false;
        }
    }
}
