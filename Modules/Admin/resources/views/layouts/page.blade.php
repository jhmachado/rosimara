<!DOCTYPE html>
<html lang="pt-br">
<head>
  <title> Protótipo - Admin </title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Admin area">
  <meta name="author" content="Author">
  <title> Admin </title>
  <!-- Favicon -->
  <link href="{{ url('/assets/admin/img/brand/favicon.png') }}" rel="icon" type="image/png">
  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- Icons -->
  <link href="{{ url('/assets/admin/nucleo/css/nucleo.css') }}" rel="stylesheet">
  <link href="{{ url('/assets/admin/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
  <!-- Argon CSS -->
  <link type="text/css" href="{{ url('/assets/admin/css/argon.min.css') }}" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{ url('/assets/admin/css/main.css') }}">
</head>
<body>
  
  <!-- Main content -->
  <div class="main-content">
    @yield('content')
  </div>

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  
  <!-- Argon JS -->
  <script src="{{ url('/assets/admin/js/argon.min.js') }}"></script>
</body>

</html>
