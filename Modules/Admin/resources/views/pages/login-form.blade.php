@extends('admin::layouts.page')

@section('content')
  <div class="main-content">
    <!-- Header -->
    <div class="header bg-gradient-primary py-7 py-lg-8">
      <div class="container">
        <div class="header-body text-center mb-7">
          <div class="row justify-content-center">
            <div class="col-lg-5 col-md-6">
              <h1 class="text-white">Bem vinda!</h1>
            </div>
          </div>
        </div>
      </div>
      <div class="separator separator-bottom separator-skew zindex-100">
        <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
          <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
        </svg>
      </div>
    </div>
    <!-- Page content -->
    <div class="container mt--8 pb-5">
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-7">
          <div class="card bg-secondary shadow border-0">
            <div class="card-header bg-transparent pb-5">
              <div class="text-muted text-center mt-2 mb-3">
                <h2>Use suas credenciais para logar na área de administação do site.</h2>
              </div>
            </div>

            <div class="card-body px-lg-5 py-lg-5">
              <form action="{{ route('admin.login.check') }}" method="post" role="form">
                {{ csrf_field() }}

                <div class="form-group mb-3 {{ ($errors->first('login')) ? 'has-danger' : '' }}">
                  <input class="form-control {{ ($errors->first('login')) ? 'is-invalid' : '' }}"
                         placeholder="Login"
                         type="text"
                         name="login"
                         value="{{ old('login', $login) }}">
                  @if ($errors->first('login'))
                    <div class="invalid-feedback">
                      {{ $errors->first('login') }}
                    </div>
                  @endif
                </div>
                <div class="form-group mb-3 {{ ($errors->first('login')) ? 'has-danger' : '' }}">
                  <input class="form-control {{ ($errors->first('senha')) ? 'is-invalid' : '' }}"
                         placeholder="Senha"
                         type="password"
                         name="senha">
                  @if ($errors->first('senha'))
                    <div class="invalid-feedback">
                      {{ $errors->first('senha') }}
                    </div>
                  @endif
                </div>
                <div class="text-center">
                  <button type="submit" class="btn btn-primary my-4"> Login </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
