@extends('admin::layouts.master')

@section('content')
<!-- Formulário -->
<div class="container-fluid mt--7">
	<form action="{{ route('admin.negocio.form-post', ['slug' => $slug]) }}" method="post">
		{{ csrf_field() }}
		<div class="row">
			<div class="card bg-secondary shadow mx-3" style="width: 100%">
		<div class="card-header bg-white border-0">
			<div class="row align-items-center">
				@if (empty($slug))
					<h3 class="mx-2"> Novo tipo de negócio </h3>
				@else
					<h3 class="mx-2"> Alterar tipo de negócio: {{ $titulo }} </h3>
				@endif
			</div>
		</div>
		<div class="card-body">
			<div class="row">
				<div class="col-lg-6">
					<div class="form-group">
						<label class="form-control-label" for="titulo">Título</label>
						<input
							type="text"
							name="titulo"
							id="titulo"
							class="form-control {{ ($errors->first('titulo')) ? 'is-invalid' : '' }}"
							placeholder="Título"
							value="{{ old('titulo', $titulo) }}"
						/>
						@if ($errors->first('titulo'))
						  <div class="invalid-feedback">
							{{ $errors->first('titulo') }}
						  </div>
						@endif
					</div>
				</div>
				<div class="col-lg-6">
					<div class="form-group">
						<label class="form-control-label" for="status">Status</label>
						<select name="status" id="status" class="form-control {{ ($errors->first('status')) ? 'is-invalid' : '' }}">
							<option value="1" {{ old('status', $status) == "1" ? 'selected=selected' : '' }}> Ativo </option>
							<option value="0" {{ old('status', $status) == "0" ? 'selected=selected' : '' }}> Inativo </option>
						</select>
						@if ($errors->first('status'))
						  <div class="invalid-feedback">
							{{ $errors->first('status') }}
						  </div>
						@endif
					</div>
				</div>
			</div>
		</div>
		<div class="card-footer py-4 text-right">
			<a href="{{ route('admin.negocio.lista') }}" class="btn btn-2 btn-danger">
				Cancelar
			</a>

			<button type="submit" class="btn btn-2 btn-success">
				{{ (empty($slug)) ? 'Cadastrar' : 'Alterar' }}
			</button>
		</div>
		</div>
	</div>
	</form>
</div>
@endsection
