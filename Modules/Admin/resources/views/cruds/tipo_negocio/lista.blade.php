@extends('admin::layouts.master')

@section('content')
<!-- Filtros -->
<div class="container-fluid mt--7">
	<div class="row">
		<div class="col">
			<div class="card shadow">
		<div class="card-header border-0">
			<div class="row">
					<div class="col-11">
					<h3 class="mb-0 pull-left"> Tipos de negócios </h3>
				</div>
					<div class="col-1 text-right">
				<a href="{{ route('admin.negocio.form', ['slug' => 'novo']) }}" class="btn btn-2 btn-success active pull-right">
					Novo
				</a>
				</div>
			</div>
		</div>
		<div>
			<form action="{{ route('admin.negocio.filtrar') }}" method="post">
			{{ csrf_field() }}
				<div class="row p-4">
						<div class="col-md-6">
						<div class="form-group">
							<label class="form-control-label" for="filtro-titulo">Título</label>
							<input type="text" class="form-control" id="filtro-titulo" placeholder="Título">
						</div>
						</div>
						<div class="col-md-6">
						<div class="form-group">
								<label class="form-control-label" for="filtro-status">Status</label>
							<select class="form-control" id="filtro-status" name="status">
								<option value="">Todos</option>
								<option value="1">Ativo</option>
								<option value="0">Inativo</option>
							</select>
						</div>
						</div>
							<div class="col-md-12 text-right">
								<button type="submit" class="btn btn-2 btn-primary active">
									Filtrar
								</button>
							</div>
					</div>
			</form>
		</div>
		</div>
		</div>
	</div>
</div>

<!-- Lista -->
<div class="container-fluid mt-2">
	<div class="row">
		<div class="col">
			<div class="card shadow">
				<div class="card-header border-0">
					<h3 class="mb-0"> Tipos de negócios </h3>
				</div>
				<div class="table-responsive">
					<table class="table align-items-center table-flush" width="100%">
						<thead class="thead-light">
							<tr>
								<th class="text-center" width="3%" scope="col"></th>
								<th class="text-left" width="67%" scope="col">Título</th>
								<th class="text-center" width="20%" scope="col">Status</th>
								<th class="text-center" width="10%" scope="col">Ações</th>
							</tr>
						</thead>
						<tbody>
							@foreach($lista as $item)
								<tr>
									<th class="text-left" scope="row">
										<div class="custom-control custom-checkbox mb-3">
											<input class="custom-control-input" id="customCheck1" type="checkbox">
											<label class="custom-control-label" for="customCheck1"></label>
										</div>
									</th>
									<td class="text-left"> {{ $item->titulo }} </td>
									<td class="text-center"> {{ ($item->status === 1) ? 'Ativo' : 'Invativo' }} </td>
									<td class="text-center">
										<a href="{{ route('admin.negocio.form', ['slug' => $item->slug]) }}"
										   class="btn btn-icon btn-2 btn-warning active"
										   role="button" aria-pressed="true">
											EDITAR
										</a>
										<a href="{{ route('admin.negocio.delete', ['slug' => $item->slug]) }}"
										   class="btn btn-icon btn-2 btn-danger active"
										   role="button" aria-pressed="true">
											EXCLUIR
										</a>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="card-footer py-4">

				</div>
			</div>
		</div>
	</div>
</div>
@endsection
