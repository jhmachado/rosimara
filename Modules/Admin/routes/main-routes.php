<?php

Route::group([
	'namespace' => 'Modules\Admin\Http\Controllers',
	'name' => 'admin.routes',
	'prefix' => 'admin',
	'middleware' => ['web']
], function() {
	Route::get('/login', 'LoginController@getForm')->name('admin.login');
    Route::post('/login', 'LoginController@checkCredentials')->name('admin.login.check');
	
	Route::group([
		'middleware' => ['auth:admin'],
	], function() {
		
		Route::get('/', 'DashboardController@getIndex')->name('admin.dashboard');
	    Route::get('/imagens', 'ImagensController@getImagem')->name('admin.imagens');
	    Route::get('/logout', 'LoginController@logout')->name('admin.logout');
	    
	    Route::get('/tipos-de-negocio', 'TipoNegocioController@getLista')->name('admin.negocio.lista');
	    Route::post('/tipos-de-negocio', 'TipoNegocioController@getLista')->name('admin.negocio.filtrar');
	    
	    Route::get('/tipos-de-negocio/{slug}', 'TipoNegocioController@getForm')->name('admin.negocio.form');
	    Route::post('/tipos-de-negocio/{slug}', 'TipoNegocioController@postForm')->name('admin.negocio.form-post');

	    Route::get('/tipos-de-negocio/deletar/{slug}', 'TipoNegocioController@deletarRegistro')->name('admin.negocio.delete');
	    Route::post('/tipos-de-negocio/deletar', 'TipoNegocioController@deletarLista')->name('admin.negocio.delete.multiple');

	});
});
