<?php

namespace Modules\Admin\Services;

use Modules\Admin\Models\Usuario;
use Modules\Admin\Repositories\Interfaces\UsuarioInterface;

/**
 * Class LoginService
 * 
 * @package Modules\Admin\Services
 */
class LoginService
{
	
	/** UsuarioInterface */
	private $usuarioRepository;

	/**
	 * @param UsuarioInterface $usuarioRepository
	 */
	public function __construct(UsuarioInterface $usuarioRepository)
	{
		$this->usuarioRepository = $usuarioRepository;
	}

	/**
	 * @param string $login
	 * @param string $senha
	 */
	public function buscarUsuarioPorLoginESenha(string $login, string $senha): ?Usuario
	{
		return $this->usuarioRepository->buscarUsuarioPorLoginESenha($login, $senha);
	}
}