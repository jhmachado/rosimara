<?php

namespace Modules\Admin\Services;

use Illuminate\Database\Eloquent\Collection;
use Modules\Admin\Models\Slug;
use Modules\Admin\Repositories\Interfaces\SlugInterface;

/**
 * Class SlugService
 * 
 * @package Modules\Admin\Services
 */
class SlugService
{ 
	/** SlugInterface */
	private $slugRepository;

	public function __construct(SlugInterface $slugRepository)
	{
		$this->slugRepository = $slugRepository;
	}

	public function salvarSlug(string $reference, string $reference_id, string $key): Slug
	{
		return $this->slugRepository->salvarSlug($reference, $reference_id, $key);
	}

	public function deletarRegistroPorSlug(string $slug): bool
	{
		return $this->slugRepository->deletarRegistroPorSlug($slug);
	}

	public function deletarSlugsPorIdsDasReferencias(array $ids): bool
	{
		return $this->slugRepository->deletarSlugsPorIdsDasReferencias($ids);
	}
}
