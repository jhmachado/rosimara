<?php

namespace Modules\Admin\Services;

use Illuminate\Database\Eloquent\Collection;
use Modules\Admin\Models\TipoNegocio;
use Modules\Admin\Repositories\Interfaces\TipoNegocioInterface;

/**
 * Class TipoNegocioService
 * 
 * @package Modules\Admin\Services
 */
class TipoNegocioService
{	
	/** TipoNegocioInterface */
	private $tipoNegocioRepository;

	/** SlugService */
	private $slugService;

	public function __construct(TipoNegocioInterface $tipoNegocioRepository, SlugService $slugService)
	{
		$this->tipoNegocioRepository = $tipoNegocioRepository;
		$this->slugService = $slugService;
	}

	public function filtrarTiposDeNegocio(array $filtros, int $pagina)
	{
		return $this->tipoNegocioRepository->filtrarTiposDeNegocio($filtros, $pagina);
	}

	public function buscarTiposDeTipoNegocio(): Collection
	{
		return $this->tipoNegocioRepository->buscarTiposDeNegocio();
	}

	public function salvarTipoDeNegocio(array $input, string $slug): TipoNegocio
	{
		$tipoDeNegocio = $this->buscarTipoDeNegocioPorSlug($slug);
		$input = $this->tratarFormulario($input, $tipoDeNegocio);

		$tipoDeNegocioSalvo = $this->tipoNegocioRepository->salvarTipoDeNegocio($tipoDeNegocio, $input);
		$this->slugService->salvarSlug('tipo_negocio', $tipoDeNegocioSalvo->id, str_slug($tipoDeNegocioSalvo->titulo));

		return $tipoDeNegocio;
	}

	public function tratarFormulario(array $input, TipoNegocio $tipoNegocioOriginal): array
	{
		$retorno['titulo'] = array_get($input, 'titulo', $tipoNegocioOriginal->titulo ?? '');
		$retorno['status'] = array_get($input, 'status', $tipoNegocioOriginal->status ?? 1);
		$retorno['slug'] = array_get($input, 'slug', $tipoNegocioOriginal->slug ?? 'novo');
		return $retorno;
	}

	public function buscarTipoDeNegocioPorSlug(string $slug): TipoNegocio
	{
		return $this->tipoNegocioRepository->buscarTipoDeNegocioPorSlug($slug);
	}

	public function deletarTipoDeNegocioPorSlug(string $slug): void
	{
		$this->tipoNegocioRepository->deletarTipoDeNegocioPorSlug($slug);
		$this->slugService->deletarRegistroPorSlug($slug);
	}

	public function deletarTiposDeNegocioPorIds(array $ids): void
	{
		$this->deletarTiposDeNegocioPorIds($ids);
		$this->slugService->deletarSlugsPorIdsDasReferencias($ids);
	}
}
