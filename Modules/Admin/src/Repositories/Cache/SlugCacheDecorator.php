<?php

namespace Modules\Admin\Repositories\Cache;

use Modules\Admin\Models\Slug;
use Modules\Admin\Repositories\Interfaces\SlugInterface;
use Modules\Support\Cache\CacheInterface;
use Support\Cache\AbstractCacheDecorator;

class SlugCacheDecorator extends AbstractCacheDecorator implements SlugInterface
{
    public function __construct(SlugInterface $repository, CacheInterface $cache)
    {
        $this->repository = $repository;
        $this->cache = $cache;
    }

    public function consultarSlug(string $slug): ?Slug
    {
        return $this->getDataIfExistCache(__FUNCTION__, func_get_args());
    }

    public function consultarSlugPorReferencia(string $reference, int $id)
    {
        return $this->getDataIfExistCache(__FUNCTION__, func_get_args());
    }

	public function salvarSlug(string $reference, string $reference_id, string $key)
	{
		$this->flushCacheAndUpdateData(__FUNCTION__, func_get_args());
	}
}