<?php

namespace Modules\Admin\Repositories\Cache;

use Illuminate\Database\Eloquent\Collection;
use Modules\Admin\Models\TipoNegocio;
use Modules\Admin\Repositories\Interfaces\TipoNegocioInterface;
use Modules\Support\Cache\CacheInterface;
use Support\Cache\AbstractCacheDecorator;

class TipoNegocioCacheDecorator extends AbstractCacheDecorator implements TipoNegocioInterface
{
	public function __construct(TipoNegocioInterface $repository, CacheInterface $cache)
	{
		$this->repository = $repository;
		$this->cache = $cache;
	}

	public function buscarTiposDeNegocio(): ?Collection
	{
		return $this->getDataIfExistCache(__FUNCTION__, func_get_args());
	}

	public function buscarTipoDeNegocioPorSlug(string $slug): ?TipoNegocio
	{
		return $this->getDataIfExistCache(__FUNCTION__, func_get_args());
	}

	public function salvarTipoDeNegocio(TipoNegocio $tipoDeTipoNegocio, array $input): TipoNegocio
	{
		$this->flushCacheAndUpdateData(__FUNCTION__, func_get_args());
	}
}
