<?php

namespace Modules\Admin\Repositories\Cache;

use Modules\Admin\Models\Usuario;
use Modules\Admin\Repositories\Interfaces\UsuarioInterface;
use Modules\Support\Cache\CacheInterface;
use Support\Cache\AbstractCacheDecorator;

class UsuarioCacheDecorator extends AbstractCacheDecorator implements UsuarioInterface
{
    public function __construct(UsuarioInterface $repository, CacheInterface $cache)
    {
        $this->repository = $repository;
        $this->cache = $cache;
    }

    public function buscarUsuarioPorLoginESenha(string $login, string $senha): ?Usuario
    {
        return $this->getDataIfExistCache(__FUNCTION__, func_get_args());
    }
}