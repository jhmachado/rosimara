<?php

namespace Modules\Admin\Repositories\Interfaces;

use Modules\Admin\Models\Usuario;
use Modules\Support\Repositories\RepositoryInterface;

interface UsuarioInterface
{
	public function buscarUsuarioPorLoginESenha(string $login, string $senha): ?Usuario;
}