<?php

namespace Modules\Admin\Repositories\Interfaces;

use Modules\Admin\Models\Slug;
use Modules\Support\Repositories\RepositoryInterface;

interface SlugInterface  extends RepositoryInterface
{
    public function consultarSlug(string $slug): ?Slug;

    public function consultarSlugPorReferencia(string $reference, int $id);

	public function salvarSlug(string $reference, string $reference_id, string $key);

	public function deletarRegistroPorSlug(string $slug): bool;

	public function deletarSlugsPorIdsDasReferencias(string $reference, array $ids): bool;
}
