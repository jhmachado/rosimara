<?php

namespace Modules\Admin\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Collection;
use Modules\Admin\Models\TipoNegocio;

interface TipoNegocioInterface
{
	public function buscarTiposDeNegocio(): Collection;

	public function buscarTipoDeNegocioPorSlug(string $slug): TipoNegocio;

	public function salvarTipoDeNegocio(TipoNegocio $tipoDeTipoNegocio, array $input): TipoNegocio;

	public function deletarTipoDeNegocioPorSlug(string $slug): bool;

	public function deletarTiposDeNegocioPorIds(array $ids): bool;

	public function filtrarTiposDeNegocio(array $filtros, int $pagina);
}
