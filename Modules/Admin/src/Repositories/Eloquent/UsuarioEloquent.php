<?php

namespace Modules\Admin\Repositories\Eloquent;

use Hash;
use Modules\Admin\Models\Usuario;
use Modules\Admin\Repositories\Interfaces\UsuarioInterface;
use Modules\Support\Repositories\AbstractRepository;

class UsuarioEloquent extends AbstractRepository implements UsuarioInterface
{
    public function buscarUsuarioPorLoginESenha(string $login, string $senha): ?Usuario
    {    	
        $usuario = Usuario::where('login', $login)
        	->where('status', 1)
        	->first();

    	return (Hash::check($senha, $usuario->senha))
    		? $usuario
    		: null;
    }
}