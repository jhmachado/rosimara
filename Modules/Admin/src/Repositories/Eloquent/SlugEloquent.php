<?php

namespace Modules\Admin\Repositories\Eloquent;

use Modules\Admin\Models\Slug;
use Modules\Admin\Repositories\Interfaces\SlugInterface;
use Modules\Support\Repositories\AbstractRepository;

class SlugEloquent extends AbstractRepository implements SlugInterface
{
    public function consultarSlug(string $slug): ?Slug
    {
        return Slug::where('key', $slug)->first();
    }

    public function consultarSlugPorReferencia(string $reference, int $id)
    {
        return Slug::where('reference', $reference)->where('reference_id', $id)->first();
    }

	public function salvarSlug(string $reference, string $reference_id, string $key)
	{
		return Slug::firstOrCreate([
			'reference' => $reference,
			'reference_id' => $reference_id,
		], [
			'key' => $key,
		]);
	}

	public function deletarRegistroPorSlug(string $slug): bool
	{
		return Slug::where('key', $slug)->delete();
	}

	public function deletarSlugsPorIdsDasReferencias(string $reference, array $ids): bool
	{
		return Slug::where('reference', $reference)
			->whereIn('reference_id', $ids)
			->delete();
	}
}