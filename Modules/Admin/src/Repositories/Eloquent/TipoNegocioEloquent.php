<?php

namespace Modules\Admin\Repositories\Eloquent;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Modules\Admin\Models\Slug;
use Modules\Admin\Models\TipoNegocio;
use Modules\Admin\Repositories\Interfaces\TipoNegocioInterface;
use Modules\Support\Repositories\AbstractRepository;
use Modules\Support\Supports\Constantes;

class TipoNegocioEloquent extends AbstractRepository implements TipoNegocioInterface
{
	public function filtrarTiposDeNegocio(array $filtros, int $pagina) {
		$query = TipoNegocio::query();

		if (!empty($filtros['titulo'])) {
			$query->where('titulo', 'like', "%{$filtros['titulo']}%");
		}

		if (!empty($filtros['status'])) {
			$query->where('status', $filtros['status']);
		}

		return $query->paginate(Constantes::getLimitePorPaginaAdmin(), ['*'], 'tipos_de_negocio', $pagina);
	}

	public function buscarTiposDeNegocio(): Collection
	{
		return TipoNegocio::where('status', 1)->get();
	}

	public function buscarTipoDeNegocioPorSlug(string $slugKey): TipoNegocio
	{
		if (empty($slugKey)) {
			return new TipoNegocio();
		}

		$slug = Slug::where('reference', 'tipo_negocio')
			->where('key', $slugKey)
			->first();

		if (!$slug) {
			return new TipoNegocio();
		}

		try {
			return TipoNegocio::findOrFail($slug->reference_id);
		} catch (ModelNotFoundException $e) {
			return new TipoNegocio();
		}
	}

	public function salvarTipoDeNegocio(TipoNegocio $tipoDeTipoNegocio, array $input): TipoNegocio
	{
		$tipoDeTipoNegocio->titulo = $input['titulo'];
		$tipoDeTipoNegocio->status = $input['status'];
		$tipoDeTipoNegocio->save();
		return $tipoDeTipoNegocio;
	}

	public function deletarTipoDeNegocioPorSlug(string $slug): bool
	{
		$tipoDeNegocio = $this->buscarTipoDeNegocioPorSlug($slug);
		return (!empty($tipoDeNegocio->id))
			? $tipoDeNegocio->delete()
			: false;
	}

	public function deletarTiposDeNegocioPorIds(array $ids): bool
	{
		return TipoNegocio::whereIn('id', $ids)->delete();
	}
}
