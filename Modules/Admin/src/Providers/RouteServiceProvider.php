<?php

namespace Modules\Admin\Providers;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
	public function boot()
	{
		$this->loadRoutesFrom(base_path('Modules/Admin/routes/main-routes.php'));
	}
}
