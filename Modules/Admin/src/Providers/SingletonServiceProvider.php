<?php

namespace Modules\Admin\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Support\Cache\Cache;
use Modules\Admin\Models\Slug;
use Modules\Admin\Repositories\Cache\SlugCacheDecorator;
use Modules\Admin\Repositories\Eloquent\SlugEloquent;
use Modules\Admin\Repositories\Interfaces\SlugInterface;
use Modules\Admin\Models\Usuario;
use Modules\Admin\Repositories\Cache\UsuarioCacheDecorator;
use Modules\Admin\Repositories\Eloquent\UsuarioEloquent;
use Modules\Admin\Repositories\Interfaces\UsuarioInterface;
use Modules\Admin\Models\TipoNegocio;
use Modules\Admin\Repositories\Cache\TipoNegocioCacheDecorator;
use Modules\Admin\Repositories\Eloquent\TipoNegocioEloquent;
use Modules\Admin\Repositories\Interfaces\TipoNegocioInterface;

class SingletonServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        if (setting('enable_cache', false)) {
            $this->registrarComCache();
            return;
        }

        $this->registrarSemCache();
    }

    private function registrarComCache(): void
    {
        $this->app->singleton(SlugInterface::class, function () {
            return new SlugCacheDecorator(new SlugEloquent(new Slug()), new Cache($this->app['cache'], SlugEloquent::class));
        });

        $this->app->singleton(UsuarioInterface::class, function () {
            return new UsuarioCacheDecorator(new UsuarioEloquent(new Usuario()), new Cache($this->app['cache'], UsuarioEloquent::class));
        });

        $this->app->singleton(TipoNegocioInterface::class, function () {
            return new TipoNegocioCacheDecorator(new TipoNegocioEloquent(new TipoNegocio()), new Cache($this->app['cache'], TipoNegocioEloquent::class));
        });
    }

    private function registrarSemCache(): void
    {
        $this->app->singleton(SlugInterface::class, function () {
            return new SlugEloquent(new Slug());
        });

        $this->app->singleton(UsuarioInterface::class, function () {
            return new UsuarioEloquent(new Usuario());
        });

        $this->app->singleton(TipoNegocioInterface::class, function () {
            return new TipoNegocioEloquent(new TipoNegocio());
        });
    }
}
