<?php

namespace Modules\Admin\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Admin\Http\Middlewares\Authenticate;
use Modules\Support\src\Supports\Helper;

class AdminServiceProvider extends ServiceProvider
{
	/**
     * @var \Illuminate\Foundation\Application
     */
    protected $app;

	public function register()
	{
        $router = $this->app['router'];
        $router->aliasMiddleware('auth', Authenticate::class);
		$this->loadViewsFrom(__DIR__ . '/../../resources/views', 'admin');
	}

	public function boot()
	{
        $this->app->register(SingletonServiceProvider::class);
        $this->app->register(RouteServiceProvider::class);

        Helper::autoload(__DIR__ . '/../../helpers');
        $this->loadMigrationsFrom(base_path('Modules/Admin/database/migrations'));
	}
}
