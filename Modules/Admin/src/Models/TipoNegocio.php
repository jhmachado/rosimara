<?php

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Traits\SlugTrait;

/**
 * Class TipoNegocio
 * 
 * @package Modules\Admin\Models
 */
class TipoNegocio extends Model
{
	use SoftDeletes;
	use SlugTrait;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'tipo_negocio';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'titulo',
		'status',
	];

	public function imoveis()
	{
		return $this->hasMany(Imovel::class, 'negocio_id');
	}
}
