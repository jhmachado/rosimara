<?php

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ImovelDetalhe
 * 
 * @package Modules\Admin\Models
 */
class ImovelDetalhe extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'imovel_detalhe';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'imovel_id',
        'titulo',
        'icone',
        'ordem',
        'status',
    ];

    public function imovel()
    {
    	return $this->belongsTo(Imovel::class);
    }
}
