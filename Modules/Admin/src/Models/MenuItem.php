<?php

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MenuItem
 * 
 * @package Modules\Admin\Models
 */
class MenuItem extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'menu_item';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titulo',
        'url',
        'target',
        'status',
    ];

    public function menu()
    {
    	return $this->belongsTo(Menu::class);
    }
}
