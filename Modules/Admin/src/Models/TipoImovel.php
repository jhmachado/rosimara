<?php

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TipoImovel
 * 
 * @package Modules\Admin\Models
 */
class TipoImovel extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tipo_imovel';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titulo',
        'status',
    ];

    public function imoveis()
    {
    	return $this->hasMany(Imovel::class, 'tipo_imovel_id');
    }
}
