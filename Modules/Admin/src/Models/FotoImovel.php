<?php

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

/**
 * Class FotoImovel
 * 
 * @package Modules\Admin\Models
 */
class FotoImovel extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'foto_imovel';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'imovel_id',
        'foto_id',
        'principal',
        'ordem',
        'status',
    ];

    public function imovel()
    {
    	return $this->belongsTo(Imovel::class);
    }
 
    public function foto()
    {
    	return $this->belongsTo(Foto::class);
    }
}
