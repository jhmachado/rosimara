<?php

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Menu
 * 
 * @package Modules\Admin\Models
 */
class Menu extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'menu';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'titulo',
        'slug',
        'status',
    ];

    public function itens()
    {
    	return $this->hasMany(MenuItem::class, 'menu_id');
    }
}
