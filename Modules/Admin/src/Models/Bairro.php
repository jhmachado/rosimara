<?php

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Bairro
 * 
 * @package Modules\Admin\Models
 */
class Bairro extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bairro';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'cidade_id',
        'nome',
        'status',
    ];

    public function imoveis()
    {
    	return $this->hasMany(Imovel::class, 'bairro_id');
    }
 
    public function cidade()
    {
    	return $this->belongsTo(Cidade::class);
    }
}
