<?php

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Traits\ImovelTrait;
use Modules\Admin\Traits\SlugTrait;

/**
 * Class Imovel
 * 
 * @package Modules\Admin\Models
 */
class Imovel extends Model
{
    use SoftDeletes;
    use SlugTrait;
    use ImovelTrait;

    protected $reference = 'imovel';

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'imovel';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tipo_negocio_id',
        'tipo_imovel_id',
        'cidade_id',
        'bairro_id',
        'titulo',
        'descricao',
        'endereco',
        'preco',
        'destaque',
        'status',
    ];

    public function tipoNegocio()
    {
    	return $this->belongsTo(TipoNegocio::class);
    }

    public function tipoImovel()
    {
    	return $this->belongsTo(TipoImovel::class);
    }

    public function detalhes()
    {
    	return $this->hasMany(ImovelDetalhe::class, 'imovel_id');
    }

    public function fotos()
    {
        return $this->hasMany(FotoImovel::class, 'imovel_id');
    }

    public function cidade()
    {
        return $this->belongsTo(Cidade::class);
    }

    public function bairro()
    {
        return $this->belongsTo(Bairro::class);
    }
}
