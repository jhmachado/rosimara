<?php

namespace Modules\Admin\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cidade';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome',
        'uf',
        'status',
    ];

    public function imoveis()
    {
    	return $this->hasMany(Imovel::class, 'cidade_id');
    }
 
    public function cidade()
    {
    	return $this->belongsTo(Cidade::class);
    }
}
