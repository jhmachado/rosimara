<?php

namespace Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class TipoNegocioDeletarListaRequest
 *
 * @package Modules\Admin\Http\Requests
 */
class TipoNegocioDeletarListaRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
	
	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'ids' => 'required|array',
		];
	}
	
	public function messages()
	{
		return [
			'titulo.array' => 'Selecione pelo menos um tipo de negócio para excluir!',
		];
	}
}
