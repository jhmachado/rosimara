<?php

namespace Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class TipoNegocioRequest
 * 
 * @package Modules\Admin\Http\Requests
 */
class TipoNegocioRequest extends FormRequest
{
  /**
   * Determine if the user is authorized to make this request.
   *
   * @return bool
   */
  public function authorize()
  {
    return true;
  }

  /**
   * Get the validation rules that apply to the request.
   *
   * @return array
   */
  public function rules()
  {
    return [
      'titulo' => 'required|max:255',
      'status' => 'required',
    ];
  }

  public function messages()
  {
  	return [
  		'titulo.required' => 'Por favor, preencha o campo titulo',
  		'status.required' => 'Por favor, selecione um status'
  	];
  }
}
