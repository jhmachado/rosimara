<?php

namespace Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

/**
 * Class LoginRequest
 * 
 * @package Modules\Admin\Http\Requests
 */
class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'login' => 'required',
            'senha' => 'required',
        ];
    }

    public function messages()
    {
    	return [
    		'login.required' => 'Por favor, preencha o campo login',
    		'senha.required' => 'Por favor, preencha o campo senha'
    	];
    }
}