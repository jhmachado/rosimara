<?php

namespace Modules\Admin\Http\Controllers;

use Symfony\Component\HttpFoundation\StreamedResponse;

class ImagensController extends BaseController
{
    public function getImagem(): StreamedResponse
    {
        return response()->file(public_path('assets/admin/imagens/avatar.svg'));
    }
}