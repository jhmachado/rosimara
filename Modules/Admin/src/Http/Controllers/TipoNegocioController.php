<?php

namespace Modules\Admin\Http\Controllers;

use Illuminate\Http\Request;
use Modules\Admin\Http\Requests\TipoNegocioDeletarListaRequest;
use Modules\Admin\Http\Requests\TipoNegocioRequest;
use Modules\Admin\Services\TipoNegocioService;

/**
 * Class TipoNegocioController
 * 
 * @package Modules\Admin\Http\Controllers
 */
class TipoNegocioController extends BaseController
{
	public function getLista(TipoNegocioService $tipoNegocioService, Request $request)
	{
		$filtros = $request->input();
		$dados_view['lista'] = $tipoNegocioService->filtrarTiposDeNegocio($filtros, $request->pagina ?? 0);
		return view('admin::cruds.tipo_negocio.lista', $dados_view);
	}

	public function getForm(TipoNegocioService $tipoNegocioService, string $slug)
	{
		$tipoNegocio = $tipoNegocioService->buscarTipoDeNegocioPorSlug($slug);
		$dados_view = $tipoNegocioService->tratarFormulario([], $tipoNegocio);
		return view('admin::cruds.tipo_negocio.form', $dados_view);
	}

	public function postForm(TipoNegocioRequest $request, TipoNegocioService $tipoNegocioService, ?string $slug)
	{
		$input = $request->input();
		$tipoNegocioService->salvarTipoDeNegocio($input, $slug);
		return redirect()->route('admin.negocio.lista');
	}

	public function deletarRegistro(TipoNegocioService $tipoNegocioService, string $slug)
	{
		$tipoNegocioService->deletarTipoDeNegocioPorSlug($slug);
		return redirect()->route('admin.negocio.lista');
	}

	public function deletarLista(TipoNegocioDeletarListaRequest $request, TipoNegocioService $tipoNegocioService)
	{
		$tipoNegocioService->deletarTiposDeNegocioPorIds($request->ids);
		return redirect()->route('admin.negocio.lista');
	}
}
