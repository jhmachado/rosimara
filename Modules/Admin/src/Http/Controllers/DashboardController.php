<?php

namespace Modules\Admin\Http\Controllers;

/**
 * Class DashboardController
 * 
 * @package Modules\Admin\Http\Controllers
 */
class DashboardController extends BaseController
{
	public function getIndex()
	{
		return view('admin::pages.dashboard');
	}
}