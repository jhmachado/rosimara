<?php

namespace Modules\Admin\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Modules\Admin\Http\Requests\LoginRequest;
use Modules\Admin\Services\LoginService;

/**
 * Class LoginController
 * 
 * @package Modules\Admin\Http\Controllers
 */
class LoginController extends BaseController
{
	public function getForm(Request $request)
	{
		$view_data['login'] = '';
		return view('admin::pages.login-form', $view_data);
	}
	
	public function checkCredentials(LoginRequest $request, LoginService $loginService)
	{
		$usuario = $loginService->buscarUsuarioPorLoginESenha($request->login, $request->senha);
		if (!$usuario) {
			return redirect()
				->route('admin.login')
				->withErrors(['login' => 'Login/senha não encontrados.']);
		}

		Auth::guard('admin')->login($usuario);
		return redirect()->route('admin.dashboard');
	}

	public function logout()
	{
		Auth::logout();
		return redirect()->route('admin.login');
	}
}