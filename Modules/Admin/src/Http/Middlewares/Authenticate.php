<?php

namespace Modules\Admin\Http\Middlewares;

use Auth;
use Closure;

/**
 * 
 */
class Authenticate
{
	/**
	 *
	 */
	public function handle($request, Closure $next, $guard = null)
	{
		if (!Auth::guard('admin')->check()) {
			return redirect()->route('admin.login');
		}

		return $next($request);
	}
}