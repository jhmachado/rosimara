<?php

namespace Modules\Admin\Traits;

use Modules\Site\Repositories\Interfaces\ImovelInterface;

trait ImovelTrait
{
    /** @var string */
    protected $fotoPrincipal;

    public function getFotoPrincipalAttribute($value): ?string
    {
        if ($this->fotoPrincipal !== null) {
            return $this->fotoPrincipal;
        }

        $fotoEncontrada = app(ImovelInterface::class)->consultarFotoPrincipal($this);
        if ($fotoEncontrada !== null) {
            $this->fotoPrincipal = $fotoEncontrada->foto->url;
        }

        return $this->fotoPrincipal;
    }
}