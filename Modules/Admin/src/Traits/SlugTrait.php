<?php

namespace Modules\Admin\Traits;

use Modules\Admin\Models\Slug;
use Modules\Admin\Repositories\Interfaces\SlugInterface;
use \Exception;

trait SlugTrait
{
	/** @var Slug|null */
	protected $slug;

	public function getSlugAttribute($value): ?string
	{
		if ($this->id === null) {
			return $this->slug = null;
		}

		if ($this->slug !== null) {
			return $this->slug;
		}

		$slugRepository = app(SlugInterface::class);
		$slugConsultado = $slugRepository->consultarSlugPorReferencia($this->table, $this->id);

		return $this->slug = $slugConsultado->key ?? '';
	}
}
