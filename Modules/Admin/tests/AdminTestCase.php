<?php

namespace Modules\Admin\tests;

use Illuminate\Database\Eloquent\Factory;
use Illuminate\Foundation\Testing\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\DB;
use Tests\CreatesApplication;

class AdminTestCase extends TestCase
{
	use CreatesApplication;
	use DatabaseMigrations;

	protected function setUp(): void
	{
		parent::setUp();
		DB::beginTransaction();
		$this->registerFactoriesPath();
	}

	private function registerFactoriesPath()
	{
		$this->app->make(Factory::class)->load(base_path('Modules/Admin/database/factories'));
	}

	protected function tearDown() {
		DB::rollback();
		parent::tearDown();
	}
}