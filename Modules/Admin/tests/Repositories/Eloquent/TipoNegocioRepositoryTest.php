<?php

namespace Modules\Admin\tests\Repositories\Eloquent;

use Modules\Admin\Models\Configuracao;
use Modules\Admin\Models\Slug;
use Modules\Admin\Models\TipoNegocio;
use Modules\Admin\Repositories\Cache\TipoNegocioCacheDecorator;
use Modules\Admin\Repositories\Eloquent\TipoNegocioEloquent;
use Modules\Admin\Repositories\Interfaces\TipoNegocioInterface;
use Modules\Admin\tests\AdminTestCase;
use PHPUnit\Framework\Assert;

class TipoNegocioRepositoryTest extends AdminTestCase
{
	/** @var TipoNegocioEloquent */
	private $tipoNegocioRepository;

	public function setUp(): void
	{
		parent::setUp();
		$this->tipoNegocioRepository = app()->make(TipoNegocioInterface::class);
		Configuracao::where('key', 'enable_cache')->update(['value' => 0]);
	}

	public function testVerifyRepositoryClass(): void
	{
		Assert::assertEquals(TipoNegocioEloquent::class, get_class($this->tipoNegocioRepository));
		Assert::assertNotEquals(TipoNegocioCacheDecorator::class, get_class($this->tipoNegocioRepository));
	}

	public function testBuscarTiposDeNegocio(): void
	{
		factory(TipoNegocio::class, 10)->create();
		$tiposDeNegocio = $this->tipoNegocioRepository->buscarTiposDeNegocio();
		Assert::assertEquals(10, $tiposDeNegocio->count());
	}

	public function testBuscarTipoDeNegocioPorSlug(): void
	{
		$tipoDeNegocioOriginal = factory(TipoNegocio::class)->create();
		$slug = factory(Slug::class)->create([
			'key' => str_slug($tipoDeNegocioOriginal->titulo),
			'reference' => $tipoDeNegocioOriginal->getTable(),
			'reference_id' => $tipoDeNegocioOriginal->id,
		]);

		$tipoDeNegocioConsultado = $this->tipoNegocioRepository->buscarTipoDeNegocioPorSlug($slug->key);
		Assert::assertEquals($tipoDeNegocioOriginal->titulo, $tipoDeNegocioConsultado->titulo);
		Assert::assertEquals($tipoDeNegocioOriginal->id, $tipoDeNegocioConsultado->id);
		Assert::assertEquals($tipoDeNegocioOriginal->status, $tipoDeNegocioConsultado->status);
	}

	public function testBuscarTipoDeNegocioPorSlugComSlugVazio()
	{
		$tipoDeNegocioConsultado = $this->tipoNegocioRepository->buscarTipoDeNegocioPorSlug('');
		Assert::assertEquals(new TipoNegocio(), $tipoDeNegocioConsultado);
	}

	public function testBuscarTipoDeNegocioPorSlugComSlugQueNaoExiste()
	{
		$tipoDeNegocioConsultado = $this->tipoNegocioRepository->buscarTipoDeNegocioPorSlug('slug_que_nao_existe');
		Assert::assertEquals(new TipoNegocio(), $tipoDeNegocioConsultado);
	}

	public function testBuscarTipoDeNegocioPorSlugComReferenciaQueNaoExiste()
	{
		$slug = factory(Slug::class)->create();
		$tipoDeNegocioConsultado = $this->tipoNegocioRepository->buscarTipoDeNegocioPorSlug($slug->key);
		Assert::assertEquals(new TipoNegocio(), $tipoDeNegocioConsultado);
	}

	public function testSalvarUmNovoTipoDeNegocio(): void
	{
		$dadosDoTipoDeNegocio = factory(TipoNegocio::class)->make()->toArray();
		$tipoDeNegocioSalvo = $this->tipoNegocioRepository->salvarTipoDeNegocio(new TipoNegocio(), $dadosDoTipoDeNegocio);
		$this->assertDatabaseHas($tipoDeNegocioSalvo->getTable(), $tipoDeNegocioSalvo->toArray());
	}

	public function testAlterarUmTipoDeNegocio()
	{
		$tipoDeNegocioExistente = factory(TipoNegocio::class)->create();
		$novosDadosDoTipoDeNegocio = factory(TipoNegocio::class)->make()->toArray();
		$tipoDeNegocioSalvo = $this->tipoNegocioRepository->salvarTipoDeNegocio($tipoDeNegocioExistente, $novosDadosDoTipoDeNegocio);
		$this->assertDatabaseHas($tipoDeNegocioSalvo->getTable(), $tipoDeNegocioSalvo->toArray());
	}

	public function testDeletarUmRegistroDeTipoDeNegocio()
	{
		$tipoDeNegocioExistente = factory(TipoNegocio::class)->create();
		factory(Slug::class)->create([
			'key' => str_slug($tipoDeNegocioExistente->titulo),
			'reference' => $tipoDeNegocioExistente->getTable(),
			'reference_id' => $tipoDeNegocioExistente->id,
		]);

		$this->tipoNegocioRepository->deletarTipoDeNegocioPorSlug($tipoDeNegocioExistente->slug);
		$this->assertSoftDeleted($tipoDeNegocioExistente->getTable(), $tipoDeNegocioExistente->toArray());
	}

	public function testDeletarUmaListaDeTiposDeNegocio()
	{
		$tiposDeNegocioExistentes = factory(TipoNegocio::class, 10)->create();
		$idsASeremDeletados = $tiposDeNegocioExistentes->pluck('id')->all();
		$this->tipoNegocioRepository->deletarTiposDeNegocioPorIds($idsASeremDeletados);

		$tipoDeNegocioDeletado = $tiposDeNegocioExistentes->pop();
		$this->assertSoftDeleted($tipoDeNegocioDeletado->getTable(), $tipoDeNegocioDeletado->toArray());
	}
}