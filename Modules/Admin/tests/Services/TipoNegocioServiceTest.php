<?php

namespace Modules\Admin\tests\Services;

use Modules\Admin\Models\Slug;
use Modules\Admin\Models\TipoNegocio;
use Modules\Admin\Services\TipoNegocioService;
use Modules\Admin\tests\AdminTestCase;
use PHPUnit\Framework\Assert;

class TipoNegocioServiceTest extends AdminTestCase
{
	/** @var TipoNegocioService */
	private $tipoNegocioService;

	public function setUp(): void
	{
		parent::setUp();
		$this->tipoNegocioService = app()->make(TipoNegocioService::class);
	}

	public function testBuscarTiposDeTipoNegocio(): void
	{
		factory(TipoNegocio::class, 10)->create();
		$tiposDeNegocio = $this->tipoNegocioService->buscarTiposDeTipoNegocio();
		Assert::assertEquals(10, $tiposDeNegocio->count());
	}

	public function testSalvarNovoTipoDeNegocio(): void
	{
		$dadosDoTipoDeNegocio = factory(TipoNegocio::class)->make()->toArray();
		$novoTipoDeNegocio = $this->tipoNegocioService->salvarTipoDeNegocio($dadosDoTipoDeNegocio, '');

		Assert::assertEquals($novoTipoDeNegocio->titulo, $dadosDoTipoDeNegocio['titulo']);
		Assert::assertEquals($novoTipoDeNegocio->status, $dadosDoTipoDeNegocio['status']);
		Assert::assertNotEmpty($novoTipoDeNegocio->id);
		$this->assertDatabaseHas('slug', ['key' => str_slug($dadosDoTipoDeNegocio['titulo'])]);
	}

	public function testSalvarTipoDeNegocioJaExistente(): void
	{
		$tipoDeNegocioExistente = factory(TipoNegocio::class)->create();
		$novosDadosDoTipoDeNegocio = factory(TipoNegocio::class)->make()->toArray();
		$slug = str_slug($tipoDeNegocioExistente->titulo);

		factory(Slug::class)->create([
			'reference' => $tipoDeNegocioExistente->getTable(),
			'reference_id' => $tipoDeNegocioExistente->id,
			'key' => $slug,
		]);

		$tipoDeNegocioAtualizado = $this->tipoNegocioService->salvarTipoDeNegocio($novosDadosDoTipoDeNegocio, $slug);
		Assert::assertEquals($tipoDeNegocioAtualizado->id, $tipoDeNegocioExistente->id);
		Assert::assertEquals($tipoDeNegocioAtualizado->titulo, $novosDadosDoTipoDeNegocio['titulo']);
		Assert::assertEquals($tipoDeNegocioAtualizado->status, $novosDadosDoTipoDeNegocio['status']);
	}

	/** @dataProvider getDadosDeFormulario */
	public function testTratarFormulario(array $dadosDoFormulario, array $resultadoEsperado): void
	{
		$formularioTratado = $this->tipoNegocioService->tratarFormulario($dadosDoFormulario, new TipoNegocio());
		Assert::assertEquals($resultadoEsperado, $formularioTratado);
	}

	public function getDadosDeFormulario(): array
	{
		return [
			// dadosDoFormulario                     resultadoEsperado
			[['titulo' => 'titulo','status' => 1],  ['titulo' => 'titulo','status' => 1, 'slug' => 'novo']],
			[[],   ['titulo' => '','status' => 1, 'slug' => 'novo']]
		];
	}
	
	public function testBuscarTipoDeTipoNegocioPorSlug(): void
	{
		$tipoDeNegocio = factory(TipoNegocio::class)->create();
		$slug = str_slug($tipoDeNegocio->titulo);

		factory(Slug::class)->create([
			'reference' => $tipoDeNegocio->getTable(),
			'reference_id' => $tipoDeNegocio->id,
			'key' => $slug,
		]);

		$tipoDeNegocioConsultado = $this->tipoNegocioService->buscarTipoDeNegocioPorSlug($slug);
		Assert::assertEquals($tipoDeNegocioConsultado->id, $tipoDeNegocio->id);
		Assert::assertEquals($tipoDeNegocioConsultado->titulo, $tipoDeNegocio->titulo);
		Assert::assertEquals($tipoDeNegocioConsultado->status, $tipoDeNegocio->status);
	}
}