<div class="card__imovel text-center">
	<a href="#servico-detalhes">
		<div class="card__imovel-imagem">
			<span class="tag tag__opaco"> Casa </span>
			<img src="./assets/imagens/imovel-2.jpg">
			<span class="tag tag__escura"> R$ 1.200,00 </span>
			<span class="tag tag__rosa"> Locação </span>
		</div>

		<div class="card__imovel-detalhes">
			<h3> Título do imóvel </h3>

			<ul class="lista">
				<li class="lista__item">
					<p class="m-0">
						<i class="fas fa-map-marker-alt"></i>
						Endereço do imovel, número
					</p>
				</li>
				<li class="lista__item">
					<p class="m-0">
						<i class="fas fa-bed"></i>
						Quartos
					</p>
				</li>
				<li class="lista__item">
					<p class="m-0">
						<i class="fas fa-car"></i>
						1 Garagem
					</p>
				</li>
				<li class="lista__item">
					<p class="m-0">
						<i class="fas fa-bath"></i>
						2 Banheiros
					</p>
				</li>
			</ul>
		</div>
	</a>
		
	<a href="imovel-1" class="card__imovel-link">
		Ver imóvel
	</a>
</div>